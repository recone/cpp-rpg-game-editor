#version 120

uniform sampler2D alpha;
uniform sampler2D texture0;
uniform sampler2D texture1;
uniform sampler2D texture2;
uniform sampler2D graymap;
uniform vec4 ambient;

//varying vec4 vertexDepth;
//uniform vec4 depthRange;

float invcol(in float p) {
return  (1.0 - p);
}

void main()
{
//float d = (length(vertexDepth.xyz) - depthRange.x) * depthRange.w;  
gl_FragColor  =texture2D(texture0, gl_TexCoord[1].st) * texture2D(alpha, gl_TexCoord[0].st).b; 
gl_FragColor +=(invcol(texture2D(alpha, gl_TexCoord[0].st).b)* texture2D(texture1, gl_TexCoord[2].st));

gl_FragColor *=texture2D(alpha, gl_TexCoord[0].st).g;
gl_FragColor +=(invcol(texture2D(alpha,gl_TexCoord[0].st).g)* texture2D(texture2, gl_TexCoord[3].st));
gl_FragColor *=texture2D(graymap, gl_TexCoord[0].st).r; 

}
