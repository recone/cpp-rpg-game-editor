/*
 * LightInfo.h
 *
 *  Created on: 3 gru 2015
 *      Author: rafal
 */

#ifndef SRC_GUI_LIGHTINFO_H_
#define SRC_GUI_LIGHTINFO_H_
#include "interfaces/InfoWindow.h"
#include "ColourPanel.h"
#include <iomanip>

namespace Gui {

class LightInfo : public InfoWindow,
  public DataConnect {
  public:
	LightInfo(MyGUI::Gui* m,Gui::ColourPanel* panel);
	virtual void setObject(Ogre::MyMovableObject* o);
	virtual void showWindow();

	void hitDiffuse(MyGUI::Widget* _sender);
	void hitSpecular(MyGUI::Widget* _sender);
	void hitDelete(MyGUI::Widget* _sender);
	void hitAccept(MyGUI::Widget* _sender);
	void hitClose(MyGUI::Widget* _sender);
	void hitSwitchCast(MyGUI::Widget* _sender);
	void hitSave(MyGUI::Widget* _sender);
	void scrollBar(MyGUI::ScrollBar* _sender, size_t _position);

	String realTostring(Real li, int prec = 9);

  private:
	void setLightRange(Ogre::Real Range);

	MyGUI::Gui* mGUI;
	MyGUI::WindowPtr wi;
	Ogre::MyMovableObject* obj = NULL;
	Ogre::Light* light = NULL;
	ColourPanel* cp;
	MyGUI::ScrollBar* scroll;
	MyGUI::TextBox* range;
	MyGUI::EditBox* edit;
	MyGUI::ButtonPtr cast;
	//MyGUI::ButtonPtr save;
	MyGUI::EditBox* lin = NULL;
	MyGUI::EditBox* qu = NULL;
  };
} /* namespace Ogre */

#endif /* SRC_GUI_LIGHTINFO_H_ */
