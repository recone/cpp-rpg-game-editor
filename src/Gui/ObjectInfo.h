/*
 * ObjectInfo.h
 *
 *  Created on: 30 cze 2015
 *      Author: rafal
 */

#ifndef SRC_GUI_OBJECTINFO_H_
#define SRC_GUI_OBJECTINFO_H_

#include <OgreRoot.h>
#include "MyGUI.h"
#include "../Stronicowanie/basics/MyMovableObject.h"
#include "../Connector/DataConnect.h"
#include "interfaces/InfoWindow.h"
#include <iostream>

namespace Gui {
class ObjectInfo : public InfoWindow,
	public DataConnect {
public:
	ObjectInfo(MyGUI::Gui* m);
	void showWindow();

	void w3ScrollDir(MyGUI::ScrollBar* _sender, size_t _position);
	void w3ScrollSize(MyGUI::ScrollBar* _sender, size_t _position);
	void w3saveSize(MyGUI::WidgetPtr _sender);
	void setObject(Ogre::MyMovableObject* o);
	void w3DeleteClick(MyGUI::WidgetPtr _sender);
	void w3TestClick(MyGUI::WidgetPtr _sender);
	void choseXYZ(MyGUI::WidgetPtr _sender);
	void w3switchShadow(MyGUI::WidgetPtr _sender);
private:
	MyGUI::Gui* mGUI;
	MyGUI::WindowPtr wi;
	Ogre::MyMovableObject* obj = NULL;
	Ogre::Quaternion qa;

	MyGUI::ScrollBar* w3scr1;
	MyGUI::ScrollBar* w3scr2;
	MyGUI::ButtonPtr w3button; //save size
	MyGUI::ButtonPtr w3del; //save size
	MyGUI::ButtonPtr w3test; //save size
	MyGUI::EditBox* w3edit;	//edit size

    /* rotate object */
	MyGUI::ButtonPtr rx;
	MyGUI::ButtonPtr ry;
	MyGUI::ButtonPtr rz;

	MyGUI::TextBox* name;
	MyGUI::TextBox* pos;
	MyGUI::ButtonPtr shadow;
	float objSize;
};

} /* namespace Gui */

#endif /* SRC_GUI_OBJECTINFO_H_ */
