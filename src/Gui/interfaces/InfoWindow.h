/*
 * InfoWindow.h
 *
 *  Created on: 4 gru 2015
 *      Author: rafal
 */

#ifndef SRC_GUI_INTERFACES_INFOWINDOW_H_
#define SRC_GUI_INTERFACES_INFOWINDOW_H_

#include <OgreRoot.h>
#include "MyGUI.h"
#include "../../Stronicowanie/basics/MyMovableObject.h"

namespace Gui {

class InfoWindow {
public:
	virtual ~InfoWindow(){};
	virtual void setObject(Ogre::MyMovableObject* o) = 0;
	virtual void showWindow() = 0;
};

} /* namespace Ogre */

#endif /* SRC_GUI_INTERFACES_INFOWINDOW_H_ */
