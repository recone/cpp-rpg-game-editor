/*
 * Interface.cpp
 *
 *  Created on: 20 cze 2015
 *      Author: rafal
 */

#include "Interface.h"

#include "../Stronicowanie/layers/LayersTextures.h"

#include <iostream>
#include <iomanip>

using namespace std;
using namespace Ogre;

namespace Gui {
Interface::Interface(MyGUI::Gui* m) {
	mGUI = m;

	//bindy
	MyGUI::ButtonPtr button2 = mGUI->findWidget<MyGUI::Button>("tb1/save");
	button2->eventMouseButtonClick += MyGUI::newDelegate(this, &Gui::Interface::tb1SaveToFile);

	MyGUI::ButtonPtr button3 = mGUI->findWidget<MyGUI::Button>("w2/zamknij");
	button3->eventMouseButtonClick += MyGUI::newDelegate(this, &Gui::Interface::tb1OknoZamknij);

	MyGUI::ButtonPtr button4 = mGUI->findWidget<MyGUI::Button>("tb1/showbox");
	button4->eventMouseButtonClick += MyGUI::newDelegate(this, &Gui::Interface::tb1ShowBox);

	MyGUI::ScrollBar* scroll1 =  mGUI->findWidget<MyGUI::ScrollBar>("tb1/bar/size");
	scroll1->eventScrollChangePosition +=MyGUI::newDelegate(this, &Gui::Interface::tb1ScrollSize);

	MyGUI::ScrollBar* scroll2 =  mGUI->findWidget<MyGUI::ScrollBar>("tb1/bar/height");
	scroll2->eventScrollChangePosition +=MyGUI::newDelegate(this, &Gui::Interface::tb1ScrollHeight);

	MyGUI::ScrollBar* scroll3 =  mGUI->findWidget<MyGUI::ScrollBar>("tb2/bar/size");
	scroll3->eventScrollChangePosition +=MyGUI::newDelegate(this, &Gui::Interface::tb1ScrollSize);

	MyGUI::ScrollBar* scroll4 =  mGUI->findWidget<MyGUI::ScrollBar>("tb2/bar/height");
	scroll4->eventScrollChangePosition +=MyGUI::newDelegate(this, &Gui::Interface::tb1ScrollHeight);

	MyGUI::ComboBox * select = mGUI->findWidget<MyGUI::ComboBox>("tb2/select");
	select->addItem("diffuse - black");
	select->addItem("alpha0 - grass");
	select->addItem("alpha1 - rock");
	select->setIndexSelected(0);
	select->setComboModeDrop(true);
	select->eventComboAccept = MyGUI::newDelegate(this, &Gui::Interface::tb2Select);

	MyGUI::ButtonPtr insert = mGUI->findWidget<MyGUI::Button>("tb3/insert");
	insert->eventMouseButtonClick += MyGUI::newDelegate(this, &Gui::Interface::tb3InsertObject);

	//bindy
	MyGUI::ButtonPtr b1 = mGUI->findWidget<MyGUI::Button>("button1");
	b1->eventMouseButtonClick += MyGUI::newDelegate(this, &Gui::Interface::clickMenu);

	MyGUI::ButtonPtr b2 = mGUI->findWidget<MyGUI::Button>("button2");
	b2->eventMouseButtonClick += MyGUI::newDelegate(this, &Gui::Interface::clickMenu);

	MyGUI::ButtonPtr b3 = mGUI->findWidget<MyGUI::Button>("button3");
	b3->eventMouseButtonClick += MyGUI::newDelegate(this, &Gui::Interface::clickMenu);

	MyGUI::ButtonPtr b4 = mGUI->findWidget<MyGUI::Button>("button4");
	b4->eventMouseButtonClick += MyGUI::newDelegate(this, &Gui::Interface::clickMenu);

	MyGUI::WindowPtr wi = mGUI->findWidget<MyGUI::Window>("w3");
	wi->setVisible(false); //eventWindowButtonPressed
	wi->eventWindowButtonPressed += MyGUI::newDelegate(this, &Gui::Interface::closeWindow);

	MyGUI::WindowPtr wi2 = mGUI->findWidget<MyGUI::Window>("w4");
	wi2->setVisible(false); //eventWindowButtonPressed
	wi2->eventWindowButtonPressed += MyGUI::newDelegate(this, &Gui::Interface::closeWindow);

	listbox = mGUI->findWidget<MyGUI::ListBox>("tb3/list");

	MyGUI::ComboBox * combo3 = mGUI->findWidget<MyGUI::ComboBox>("tb3/combo");
	combo3->setComboModeDrop(true);
	combo3->eventComboAccept = MyGUI::newDelegate(this, &Gui::Interface::tb3Select);
	tb3Select(combo3,0);

	fps = mGUI->findWidget<MyGUI::TextBox>("fps");
	loc = mGUI->findWidget<MyGUI::TextBox>("loc");

	cp  = new ColourPanel(mGUI);
	sob = new ObjectInfo(mGUI);
	lin = new LightInfo(mGUI,cp);

	MyGUI::MenuItem* it = mGUI->findWidget<MyGUI::MenuItem>("pop/ambient");
	it->eventMouseButtonClick += MyGUI::newDelegate(this, &Gui::Interface::clickShowPanelColor);
}

void Interface::clickShowPanelColor(MyGUI::WidgetPtr _sender) {
	cp->show();
	cp->setAmbientColour(getDataConnector()->mSceneMgr);
}

void Interface::clickPopup(MyGUI::WidgetPtr _sender){
	MyGUI::MenuItem* elm = (MyGUI::MenuItem*)_sender;
	if(elm) {
		elm->setItemChildVisible(true);
		if(old) {
			old->setItemChildVisible(false);
			}
		old = elm;
		}
}

void Interface::closeWindow(MyGUI::WindowPtr _sender, const std::string& _name){
	_sender->setVisible(false);
}
void Interface::setFPS(string name){
	fps->setCaption(name);
}
void  Interface::setCORD(Ogre::Vector3 co) {
	loc->setCaption("X: "+Ogre::StringConverter::toString(co.x)+" \nY: "+Ogre::StringConverter::toString(co.y)+"\nZ: "+Ogre::StringConverter::toString(co.z));
}
void Interface::clickMenu(MyGUI::WidgetPtr _sender) {
	std:string name =_sender->getName();

	MyGUI::ButtonPtr b1 = mGUI->findWidget<MyGUI::Button>("button1");
	MyGUI::ButtonPtr b2 = mGUI->findWidget<MyGUI::Button>("button2");
	MyGUI::ButtonPtr b3 = mGUI->findWidget<MyGUI::Button>("button3");
	MyGUI::ButtonPtr b4 = mGUI->findWidget<MyGUI::Button>("button4");
	b1->setColour(MyGUI::Colour::White);
	b2->setColour(MyGUI::Colour::White);
	b3->setColour(MyGUI::Colour::White);
	b4->setColour(MyGUI::Colour::White);

	_sender->setColour(MyGUI::Colour::Green);
	if(name=="button1") {
		str->setSelectedLayer(0);
		getDataConnector()->freeMode=Ogre::Flags::SystemTryb::TRYB_EDIT_MAP;
		std::cout<<"Edit map "<<endl;
		mGUI->findWidget<MyGUI::TabControl>("tabs")->selectSheetIndex(0);

	} else if(name=="button2") {
		getDataConnector()->freeMode=Ogre::Flags::SystemTryb::TRYB_EDIT_TEXTURE;
		str->setSelectedLayer(1);
		mGUI->findWidget<MyGUI::ComboBox>("tb2/select")->setItemSelectedAt(0);
		mGUI->findWidget<MyGUI::TabControl>("tabs")->selectSheetIndex(1);
		std::cout<<"Edit texture "<<endl;
	} else if(name=="button3") {
		getDataConnector()->freeMode=Ogre::Flags::SystemTryb::TRYB_EDIT_OBJECTS;
		std::cout<<"Edit objects "<<endl;
		mGUI->findWidget<MyGUI::TabControl>("tabs")->selectSheetIndex(2);
	} else {
		getDataConnector()->freeMode=Ogre::Flags::SystemTryb::TRYB_WALK;
		mGUI->findWidget<MyGUI::TabControl>("tabs")->selectSheetIndex(0);
		std::cout<<"Walk "<<endl;
		player->moveCamera();
	}
}

void Interface::tb3InsertObject(MyGUI::WidgetPtr _sender) {
	if(!listbox->isItemSelectVisible()) return; // nothing selected

	//MyGUI::ListBox* listbox = mGUI->findWidget<MyGUI::ListBox>("tb3/list");
	Ogre::String txt = listbox->getItem(listbox->getItemIndexSelected()).asUTF8();
	// Ogre::String prefix =Ogre::StringConverter::toString(Ogre::Root::getSingleton().getTimer()->getMilliseconds());

	getDataConnector()->scLoader->parseDotNode(
			txt,
			"General",
			getDataConnector()->mSceneMgr,
			getDataConnector()->ground->get3dPointer()->getPosition()
	);
}
/*
 * Wybierz layer
 */
void Interface::tb2Select(MyGUI::ComboBox* _sender, size_t _index) {
	str->setSelectedLayer(_index+1);
}

/*
 * wybierz obiektyr
 */
void Interface::tb3Select(MyGUI::ComboBox* _sender, size_t _index) {
	listbox->removeAllItems();

	Ogre::StringVectorPtr xx;
	if(_index==0) {
		xx = Ogre::ResourceGroupManager::getSingletonPtr()->listResourceNames("Objects");
	} else if(_index==1) {
		xx = Ogre::ResourceGroupManager::getSingletonPtr()->listResourceNames("OtherObjects");
	}
	Ogre::StringVector::iterator itor;
	for(itor=xx->begin(); itor!=xx->end(); itor++){
		if ( Ogre::StringUtil::endsWith((*itor), ".oxml") ) {
			listbox->addItem(*itor);
			}
		}
}

/*
 * Polaczenie do stronicowania
 */
void Interface::setStronicowanie(Stronicowanie *st) {
	this->str = st;
}

/*
 * Szerokosc okna
 */
void Interface::setWindowWidth(unsigned int wi) {
	MyGUI::IntCoord co2 = fps->getCoord();

	co2.left = wi-200;
	fps->setCoord(co2);

	MyGUI::MenuBar* mb = mGUI->findWidget<MyGUI::MenuBar>("pop");
	MyGUI::IntCoord co = mb->getCoord();
	co.width = wi;
	mb->setCoord(co);
}

/*
 * Ustawianie pędzla
 */
void Interface::tb1ScrollSize(MyGUI::ScrollBar* _sender, size_t _position) {
	getDataConnector()->painSize = _position;

	MyGUI::TextBox* lb = mGUI->findWidget<MyGUI::TextBox>("label/size"+Ogre::StringConverter::toString(mGUI->findWidget<MyGUI::TabControl>("tabs")->getItemIndexSelected()));
	lb->setCaption("Size ("+Ogre::StringConverter::toString(getDataConnector()->painSize)+")");
}
/*
 * Ustawianie pedzla
 */
void Interface::tb1ScrollHeight(MyGUI::ScrollBar* _sender, size_t _position) {
	getDataConnector()->painHeight = _position-(_sender->getScrollRange()/2);
	MyGUI::TextBox* lb = mGUI->findWidget<MyGUI::TextBox>("label/height"+Ogre::StringConverter::toString(mGUI->findWidget<MyGUI::TabControl>("tabs")->getItemIndexSelected()));
	lb->setCaption("Height ("+Ogre::StringConverter::toString(getDataConnector()->painHeight)+")");
}

/*
 * Zapisywanie zmian do plikow
 */
void Interface::tb1SaveToFile(MyGUI::WidgetPtr _sender) {
	Ogre::LogManager::getSingletonPtr()->logMessage(Ogre::LML_NORMAL," == Button hit - SaveToFile ==");

	str->getLayersTexturesPtr()->save();
	str->getTerrainPtr()->saveTerrainData();
	getDataConnector()->scSaver->saveSceneToXML();

	MyGUI::WindowPtr win = mGUI->findWidget<MyGUI::Window>("w2");
	win->setVisible(true);
  }

/*
 * Zamknij okno w2
 */
void Interface::tb1OknoZamknij(MyGUI::WidgetPtr _sender) {
	MyGUI::WindowPtr win = mGUI->findWidget<MyGUI::Window>("w2");
	win->setVisible(false);
  }

/*
 * Pokaz boxy na obiektach terenu
 */
void Interface::tb1ShowBox(MyGUI::WidgetPtr _sender) {
	 MyGUI::ButtonPtr checkbox = _sender->castType<MyGUI::Button>();
	 checkbox->setStateCheck(!checkbox->getStateCheck());
	 str->setAllBoundinBoxes(checkbox->getStateCheck());
  }

void Interface::setPlayer(Player* pl) {
	player = pl;
  }

InfoWindow* Interface::selectWindow(Ogre::MyMovableObject* ob) {
	if(ob->getJointNode()->getTyp()==Ogre::Flags::NodeType::UNKNOWN) {
		return sob;
	} else {
		return lin;
	}
}

Interface::~Interface() {
  }
} /* namespace Gui */
