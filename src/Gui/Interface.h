/*
 * Interface.h
 *
 *  Created on: 20 cze 2015
 *      Author: rafal
 */

#ifndef SRC_GUI_INTERFACE_H_
#define SRC_GUI_INTERFACE_H_

#include <OgreRoot.h>
#include <OgreEntity.h>

//mygui
#include "MyGUI.h"
#include "MyGUI_OgrePlatform.h"
#include "../Stronicowanie/Stronicowanie.h"
#include "../Connector/DataConnect.h"
#include "../Stronicowanie/Player.h"

#include "ColourPanel.h"
#include "ObjectInfo.h"
#include "LightInfo.h"

using namespace std;
using namespace Ogre;

namespace Gui {
class Interface :
	public DataConnect {
public:
	Interface(MyGUI::Gui* m);
	void tb1SaveToFile(MyGUI::WidgetPtr _sender);
	void tb1OknoZamknij(MyGUI::WidgetPtr _sender);
	void tb1ShowBox(MyGUI::WidgetPtr _sender);
	void tb1ScrollSize(MyGUI::ScrollBar* _sender, size_t _position);
	void tb1ScrollHeight(MyGUI::ScrollBar* _sender, size_t _position);
	void tb2Select(MyGUI::ComboBox* _sender, size_t _index);
	void tb3Select(MyGUI::ComboBox* _sender, size_t _index);
	void tb3InsertObject(MyGUI::WidgetPtr _sender);
	void clickMenu(MyGUI::WidgetPtr _sender);
	void clickPopup(MyGUI::WidgetPtr _sender);
	void clickShowPanelColor(MyGUI::WidgetPtr _sender);
	void setPlayer(Player* pl);
	void closeWindow(MyGUI::WindowPtr _sender, const std::string& _name);

	void setStronicowanie(Stronicowanie *st);
	void setWindowWidth(unsigned int wi);
	void setFPS(string name);
	void setCORD(Ogre::Vector3 co);
	InfoWindow* selectWindow(Ogre::MyMovableObject* ob);

	~Interface();

private:
	MyGUI::Gui* mGUI;
	Stronicowanie* str;
	MyGUI::TextBox* fps;
	MyGUI::TextBox* loc;
	Player* player;
	MyGUI::ListBox* listbox;
	MyGUI::MenuItem* old;

	ColourPanel* cp;
	ObjectInfo* sob;
	LightInfo* lin;
	};
} /* namespace Gui */

#endif /* SRC_GUI_INTERFACE_H_ */
