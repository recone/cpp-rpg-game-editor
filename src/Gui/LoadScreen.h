/*
 * LoadScreen.h
 *
 *  Created on: 30 lis 2015
 *      Author: rafal
 */

#ifndef SRC_GUI_LOADSCREEN_H_
#define SRC_GUI_LOADSCREEN_H_

//mygui
#include "MyGUI.h"

namespace Ogre {

class LoadScreen {
public:
	MyGUI::ImageBox* image;

	LoadScreen(MyGUI::Gui* mGUI) {
		MyGUI::LayoutManager::getInstance().loadLayout("loading.layout");// main layout
		image = mGUI->findWidget<MyGUI::ImageBox>("loading");
	}
	void show() {
		MyGUI::PointerManager::getInstancePtr()->hide();
		image->setVisible(true);
	}
	void hide() {
		MyGUI::PointerManager::getInstancePtr()->show();
		image->setVisible(false);
	}
};

} /* namespace Ogre */

#endif /* SRC_GUI_LOADSCREEN_H_ */
