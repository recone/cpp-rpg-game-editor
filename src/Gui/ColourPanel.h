/*!
	@file
	@author		Albert Semenov
	@date		09/2008
*/
#ifndef COLOUR_PANEL_H_
#define COLOUR_PANEL_H_

#include "MyGUI.h"
#include "MyGUI_OgrePlatform.h"
#include "../Connector/DataConnect.h"

namespace Gui
{
 class ColourPanel  :
			public DataConnect {
	public:
	    ColourPanel(MyGUI::Gui* m);
		virtual ~ColourPanel();

		void setColour(Ogre::ColourValue& _colour);
		// set color for Scene Ambient
		void setAmbientColour(Ogre::SceneManager* scene);
		// for light
		void setDiffuseColour(Ogre::Light* light);
		// for light
		void setSpecularColour(Ogre::Light* light);

		const MyGUI::Colour& getColour() const;
		MyGUI::delegates::CDelegate1<ColourPanel*> eventColourAccept;
		void hide();
		void show();
	private:
		void notifyMouseDrag(MyGUI::Widget* _sender, int _left, int _top, MyGUI::MouseButton _id);
		void notifyMouseButtonPressed(MyGUI::Widget* _sender, int _left, int _top, MyGUI::MouseButton _id);
		void notifyScrollChangePosition(MyGUI::ScrollBar* _sender, size_t _position);
		void notifyEditTextChange(MyGUI::EditBox* _sender);
		void notifyMouseButtonClick(MyGUI::Widget* _sender);
		void cancelMouseButtonClick(MyGUI::Widget* _sender);
		void updateFirst();

		void createTexture();
		void updateTexture(const MyGUI::Colour& _colour);
		void destroyTexture();

		void updateFromPoint(const MyGUI::IntPoint& _point);
		void updateFromColour(const MyGUI::Colour& _colour);

		MyGUI::Colour getSaturate(const MyGUI::Colour& _colour) const;
		float& byIndex(MyGUI::Colour& _colour, size_t _index);
	private:
		MyGUI::Gui* mGUI;
		Ogre::ColourValue incolour;

	//	ATTRIBUTE_FIELD_WIDGET_NAME(ColourPanel, mColourRect, "widget_ColourRect");
		MyGUI::ImageBox* mColourRect;

	//	ATTRIBUTE_FIELD_WIDGET_NAME(ColourPanel, mColourView, "widget_ColourView");
		MyGUI::Widget* mColourView;

	//	ATTRIBUTE_FIELD_WIDGET_NAME(ColourPanel, mImageColourPicker, "image_Picker");
		MyGUI::ImageBox* mImageColourPicker;

	//	ATTRIBUTE_FIELD_WIDGET_NAME(ColourPanel, mEditRed, "edit_Red");
		MyGUI::EditBox* mEditRed;

	//	ATTRIBUTE_FIELD_WIDGET_NAME(ColourPanel, mEditGreen, "edit_Green");
		MyGUI::EditBox* mEditGreen;

	//	ATTRIBUTE_FIELD_WIDGET_NAME(ColourPanel, mEditBlue, "edit_Blue");
		MyGUI::EditBox* mEditBlue;

	//	ATTRIBUTE_FIELD_WIDGET_NAME(ColourPanel, mScrollRange, "scroll_Range");
		MyGUI::ScrollBar* mScrollRange;

	//	ATTRIBUTE_FIELD_WIDGET_NAME(ColourPanel, mOk, "button_OK");
		MyGUI::Button* mOk;

	//	ATTRIBUTE_FIELD_WIDGET_NAME(ColourPanel, mOk, "button_OK");
		MyGUI::Button* mCancel;

		MyGUI::Colour mCurrentColour;
		MyGUI::Colour mBaseColour;

		std::vector<MyGUI::Colour> mColourRange;
		MyGUI::ITexture* mTexture;
		MyGUI::Window* wi;
		bool envAmbient = false;

		Ogre::Light* light  = NULL;
		bool diffuse;
		bool specular;
	};

} // namespace demo

#endif // COLOUR_PANEL_H_
