/*
 * LightInfo.cpp
 *
 *  Created on: 3 gru 2015
 *      Author: rafal
 */

#include "LightInfo.h"

namespace Gui {
	LightInfo::LightInfo(MyGUI::Gui* m,ColourPanel* panel) {
		mGUI = m;
		cp = panel;

		wi = mGUI->findWidget<MyGUI::Window>("w4");
		MyGUI::ButtonPtr b1 = mGUI->findWidget<MyGUI::Button>("w4/diffuse");
		b1->eventMouseButtonClick += MyGUI::newDelegate(this, &LightInfo::hitDiffuse);
		MyGUI::ButtonPtr b2 = mGUI->findWidget<MyGUI::Button>("w4/specular");
		b2->eventMouseButtonClick +=MyGUI::newDelegate(this, &LightInfo::hitSpecular);
		MyGUI::ButtonPtr b3 = mGUI->findWidget<MyGUI::Button>("w4/delete");
		b3->eventMouseButtonClick +=MyGUI::newDelegate(this, &LightInfo::hitDelete);

		MyGUI::ButtonPtr b5 = mGUI->findWidget<MyGUI::Button>("w4/accept");
		b5->eventMouseButtonClick +=MyGUI::newDelegate(this, &LightInfo::hitAccept);
		MyGUI::ButtonPtr b6 = mGUI->findWidget<MyGUI::Button>("w4/close");
		b6->eventMouseButtonClick +=MyGUI::newDelegate(this, &LightInfo::hitClose);

		MyGUI::ButtonPtr b7 = mGUI->findWidget<MyGUI::Button>("w4/save");
		b7->eventMouseButtonClick +=MyGUI::newDelegate(this, &LightInfo::hitSave);

		scroll =  mGUI->findWidget<MyGUI::ScrollBar>("w4/scroll");
		scroll->eventScrollChangePosition +=MyGUI::newDelegate(this,&LightInfo::scrollBar);
		range   = mGUI->findWidget<MyGUI::TextBox>("w4/pos");

		cast = mGUI->findWidget<MyGUI::Button>("w4/cast");
		cast->eventMouseButtonClick += MyGUI::newDelegate(this,&LightInfo::hitSwitchCast);
		edit = mGUI->findWidget<MyGUI::EditBox>("w4/edit");

		lin = mGUI->findWidget<MyGUI::EditBox>("w4/lin");
		qu  = mGUI->findWidget<MyGUI::EditBox>("w4/qu");
	}

	void LightInfo::hitDiffuse(MyGUI::Widget* _sender) {
		cp->setDiffuseColour(light);
		cp->show();
	}

	void LightInfo::hitSpecular(MyGUI::Widget* _sender) {
		cp->setSpecularColour(light);
		cp->show();
	}

	void LightInfo::hitDelete(MyGUI::Widget* _sender) {
		try {
			if(obj!=NULL) {
			Ogre::MySceneNode* mysc = obj->getJointNode();
			mysc->destroyAllAttachedMovableObjects();
			mysc->destroyAllNodes();
			obj = NULL;
			this->getDataConnector()->setMovNull();
			}
		} catch (Ogre::Exception &e) {
			cout<<e.getDescription()<<endl;
		}
		wi->setVisible(false);
	}


	void LightInfo::hitAccept(MyGUI::Widget* _sender) {
		Real range = Ogre::StringConverter::parseReal(edit->getCaption().asUTF8());
		if(range>0) {
			light->setAttenuation(range,1,Ogre::StringConverter::parseReal(lin->getCaption()),Ogre::StringConverter::parseReal(qu->getCaption()));
			scroll->setScrollPosition(0);
			scroll->setScrollRange(range);
			}
	}

	void LightInfo::hitClose(MyGUI::Widget* _sender) {
		wi->hideSmooth();
	}

	void LightInfo::setObject(Ogre::MyMovableObject* o) {
		obj = o;
		Ogre::SceneNode::ObjectIterator it = obj->getJointNode()->getAttachedObjectIterator();
		while (it.hasMoreElements()) {
		 MovableObject* pObject = static_cast<MovableObject*>(it.getNext());
		 if(pObject->getMovableType()=="Light") {
			 light= (Light*)pObject;
			 cast->setButtonPressed(light->getCastShadows());
			 lin->setCaption(realTostring(light->getAttenuationLinear()));
			 qu->setCaption(realTostring(light->getAttenuationQuadric()));
			 scrollBar(scroll,light->getAttenuationRange());
		 	 }
		}
	}
	 String LightInfo::realTostring(Real li, int prec) {
		 std::stringstream buff;
		 buff << std::fixed << std::setprecision(prec) << li;

		 return buff.str();
	 }
	 void LightInfo::setLightRange(Ogre::Real Range)
	 {
		 light->setAttenuation( Range, 1.0f, 4.5/Range, 75.0f/(Range*Range) );
	 }
	void LightInfo::scrollBar(MyGUI::ScrollBar* _sender, size_t _position) {
		setLightRange((Real)_position);
		//light->setAttenuation(_position,1,Ogre::StringConverter::parseReal(lin->getCaption()),Ogre::StringConverter::parseReal(qu->getCaption()));
		range->setCaption(Ogre::StringConverter::toString(_position));
		 lin->setCaption(realTostring(light->getAttenuationLinear()));
		 qu->setCaption(realTostring(light->getAttenuationQuadric()));
		_sender->setScrollPosition(_position);
	}
	void  LightInfo::hitSwitchCast(MyGUI::Widget* _sender) {
		MyGUI::ButtonPtr button = _sender->castType<MyGUI::Button>();
		if(button->getButtonPressed())
				button->setButtonPressed(false);
			else
				button->setButtonPressed(true);
		light->setCastShadows(button->getButtonPressed());
	}

	void LightInfo::hitSave(MyGUI::Widget* _sender) {
		light->setAttenuation(scroll->getScrollPosition(),1,Ogre::StringConverter::parseReal(lin->getCaption()),Ogre::StringConverter::parseReal(qu->getCaption()));
	}

	void LightInfo::showWindow() {
		wi->setVisible(true);
	}
}
