/*
 * ObjectInfo.cpp
 *
 *  Created on: 30 cze 2015
 *      Author: rafal
 */

#include "ObjectInfo.h"
#include "../BaseApplication.h"

namespace Gui {


	ObjectInfo::ObjectInfo(MyGUI::Gui* m) {
		mGUI = m;
		wi = mGUI->findWidget<MyGUI::Window>("w3");

		w3scr1 =  mGUI->findWidget<MyGUI::ScrollBar>("w3/dir");
		w3scr1->eventScrollChangePosition +=MyGUI::newDelegate(this,&Gui::ObjectInfo::w3ScrollDir);

		w3scr2 =  mGUI->findWidget<MyGUI::ScrollBar>("w3/size");
		w3scr2->eventScrollChangePosition +=MyGUI::newDelegate(this,&Gui::ObjectInfo::w3ScrollSize);

		w3button = mGUI->findWidget<MyGUI::Button>("w3/save");
		w3button->eventMouseButtonClick += MyGUI::newDelegate(this,&Gui::ObjectInfo::w3saveSize);

		w3del = mGUI->findWidget<MyGUI::Button>("w3/delete");
		w3del->eventMouseButtonClick += MyGUI::newDelegate(this,&Gui::ObjectInfo::w3DeleteClick);

		w3test = mGUI->findWidget<MyGUI::Button>("w3/test");
		w3test->eventMouseButtonClick += MyGUI::newDelegate(this,&Gui::ObjectInfo::w3TestClick);

		w3edit = mGUI->findWidget<MyGUI::EditBox>("w3/edit");
		rx = mGUI->findWidget<MyGUI::Button>("w3/x");
		rx->eventMouseButtonClick += MyGUI::newDelegate(this, &Gui::ObjectInfo::choseXYZ);
		//rotate over y
		ry = mGUI->findWidget<MyGUI::Button>("w3/y");
		ry->eventMouseButtonClick += MyGUI::newDelegate(this, &Gui::ObjectInfo::choseXYZ);
		//rotate over z
		rz = mGUI->findWidget<MyGUI::Button>("w3/z");
		rz->eventMouseButtonClick += MyGUI::newDelegate(this, &Gui::ObjectInfo::choseXYZ);

		name   = mGUI->findWidget<MyGUI::TextBox>("w3/name");
		pos    = mGUI->findWidget<MyGUI::TextBox>("w3/pos");

		shadow = mGUI->findWidget<MyGUI::Button>("w3/shadow");
		shadow->eventMouseButtonClick += MyGUI::newDelegate(this,&Gui::ObjectInfo::w3switchShadow);
	}

	void ObjectInfo::setObject(Ogre::MyMovableObject* o) {
	  if(obj!=o && o!=NULL) {
		qa = o->getJointNode()->getOrientation();
		name->setCaption("Name: "+o->getName());
		pos->setCaption("X: "+Ogre::StringConverter::toString(o->getJointNode()->getPosition().x,4)+", Y: "+Ogre::StringConverter::toString(o->getJointNode()->getPosition().y,4)+", Z: "+Ogre::StringConverter::toString(o->getJointNode()->getPosition().z,4));
		obj = o;
		w3scr2->setScrollPosition(w3scr2->getScrollRange()-1);
		w3edit->setCaption(Ogre::StringConverter::toString(o->getJointNode()->getScale().x));
		objSize = o->getJointNode()->getScale().x;
		shadow->setButtonPressed(obj->getCastShadows());
		}
	}
	/*
	 * Apply all params to object
	 */
	void ObjectInfo::showWindow() {
		wi->setVisible(true);
	}

	void ObjectInfo::w3ScrollDir(MyGUI::ScrollBar* _sender, size_t _position){

		float wyn = (float)_position/_sender->getScrollRange();
		obj->getJointNode()->setOrientation(qa);

		if(rx->getStateCheck())
			obj->getJointNode()->pitch(Ogre::Degree(360*wyn));
		if(ry->getStateCheck())
			obj->getJointNode()->yaw(Ogre::Degree(360*wyn));
		if(rz->getStateCheck())
			obj->getJointNode()->roll(Ogre::Degree(360*wyn));
	}

	void ObjectInfo::w3ScrollSize(MyGUI::ScrollBar* _sender, size_t _position) {
		float tmpobjSize = objSize*((float)_position/_sender->getScrollRange());
		obj->getJointNode()->setScale(tmpobjSize,tmpobjSize,tmpobjSize);
	}
	/*
	 * Apply object size
	 */
	void ObjectInfo::w3saveSize(MyGUI::WidgetPtr _sender) {
		objSize = Ogre::StringConverter::parseReal(w3edit->getCaption().asUTF8());
		w3scr2->setScrollPosition(w3scr2->getScrollRange()-1);
		obj->getJointNode()->setScale(objSize,objSize,objSize);
		Vector3 ve = obj->getJointNode()->getPosition();
	}

	/*
	 * Test object
	 */
	void ObjectInfo::w3TestClick(MyGUI::WidgetPtr _sender) {
		Vector3 c0 = obj->getWorldBoundingBox().getHalfSize();
		Vector3 npos = obj->getWorldBoundingBox().getCenter();

		Vector3 buff;
		for(int x=c0.x*-1;x<c0.x;x+=20) {
			for(int z=c0.z*-1;z<c0.z;z+=20) {
				int bx=npos.x+x;
				int bz=npos.z+z;
				Vector3 dir(0,-1,0);
				Vector2 vc = getDataConnector()->ground->getPixeltoVertexCoords(bx,bz);

				Vector3 vcr(bx,obj->getJointNode()->getPosition().y+100,bz);
				Ray ra(vcr,dir);
				MyRaycast* rc = new MyRaycast();
				rc->setRayMask(Ogre::Flags::ObjectFlags::MOVING_OBJ);
				Entity* ent;
				if(rc->raycastFromPoint(ra,buff,ent)) {
					if(obj->getName()==ent->getName())
					getDataConnector()->ground->getTerrainPtr()->setPixel(vc.x,vc.y,buff.y);
					}
				}
		}
		getDataConnector()->ground->refreshInRadius(npos.x,npos.z,true);
	}

	/*
	 * Delete object
	 */
	void ObjectInfo::w3DeleteClick(MyGUI::WidgetPtr _sender) {
		try {
			if(obj!=NULL) {
				Ogre::MySceneNode* mysc = obj->getJointNode();
				mysc->destroyAllAttachedMovableObjects();
				mysc->destroyAllNodes();
				obj = NULL;
				this->getDataConnector()->setMovNull();
				}
		} catch (Ogre::Exception &e) {
			cout<<e.getDescription()<<endl;
		}
		wi->setVisible(false);
	}

	void ObjectInfo::w3switchShadow(MyGUI::WidgetPtr _sender) {
		MyGUI::ButtonPtr button = _sender->castType<MyGUI::Button>();
		if(button->getButtonPressed())
				button->setButtonPressed(false);
			else
				button->setButtonPressed(true);

		obj->setCastShadows(shadow->getButtonPressed());
		SceneNode::ChildNodeIterator nodei = obj->getJointNode()->getChildIterator();
		while (nodei.hasMoreElements())
		{
		   SceneNode* childNode = static_cast<SceneNode*>(nodei.getNext());
		   SceneNode::ObjectIterator  iterator = childNode->getAttachedObjectIterator();
			while(iterator.hasMoreElements())
			{
			      Ogre::Entity* e = static_cast<Ogre::Entity*>(iterator.getNext());
			      e->setCastShadows(shadow->getButtonPressed());
			}

		}

	}

	void ObjectInfo::choseXYZ(MyGUI::WidgetPtr _sender) {
		 MyGUI::ButtonPtr button = _sender->castType<MyGUI::Button>();
		 qa = obj->getJointNode()->getOrientation();

		 rx->setButtonPressed(false);
		 ry->setButtonPressed(false);
		 rz->setButtonPressed(false);
		 button->setButtonPressed(true);
	}
} /* namespace Gui */
