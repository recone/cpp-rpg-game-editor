/*
 * DotSceneLoader.h
 *
 *  Created on: 11 lip 2015
 *      Author: rafal
 */

#ifndef DOTXML_DOTSCENELOADER_H_
#define DOTXML_DOTSCENELOADER_H_

// Includes
#include <Ogre.h>
#include <OgreString.h>
#include <OgreVector3.h>
#include <OgreQuaternion.h>

#include <list>
#include <iostream>
#include <string>
#include <iomanip>
#include <vector>

#include "../Stronicowanie/basics/Flags.h"

#include "../Stronicowanie/basics/ListenerEntity.h"
#include "../Stronicowanie/basics/ListenerNode.h"
#include "../Stronicowanie/basics/MySceneNode.h"
#include "../Stronicowanie/basics/MyMovableObject.h"

#include "tinyxml.h"

// Forward declarations
class TiXmlElement;
using namespace std;

namespace Ogre {
    // Forward declarations
    class SceneManager;
    class SceneNode;

    class nodeProperty {
    public:
        String nodeName;
        String propertyNm;
        String valueName;
        String typeName;

        nodeProperty(const String &node, const String &propertyName, const String &value, const String &type)
            : nodeName(node), propertyNm(propertyName), valueName(value), typeName(type) {}
    };

    class DotSceneLoader {
    public:
        DotSceneLoader() : mSceneMgr(0) {  }
        virtual ~DotSceneLoader() {}
        void parseDotNode(const String &SceneName, const String &groupName, SceneManager *yourSceneMgr, Ogre::Vector3 pos, SceneNode *pAttachNode = NULL, const String &sPrependNode = "");
        void parseDotScene(const String &SceneName, const String &groupName, SceneManager *yourSceneMgr, SceneNode *pAttachNode = NULL, const String &sPrependNode = "");

        std::list<string> getAllNodes(SceneNode* ok);
        std::list<string> getAllLights();

        void saveSceneToXML(SceneManager *SceneMgr);
        String getProperty(const String &ndNm, const String &prop);

        std::vector<nodeProperty> nodeProperties;
        std::vector<String> staticObjects;
        std::vector<String> dynamicObjects;

    protected:
        void processScene(TiXmlElement *XMLRoot);

        void getAllNodes(SceneNode* ok,std::list<string> &li);
        bool nameExists(std::list<string> &li,string name);

        void processNodes(TiXmlElement *XMLNode);
        void processExternals(TiXmlElement *XMLNode);
        void processEnvironment(TiXmlElement *XMLNode);
        void processTerrain(TiXmlElement *XMLNode);
        void processUserDataReference(TiXmlElement *XMLNode, SceneNode *pParent = 0);
        void processUserDataReference(TiXmlElement *XMLNode, Entity *pEntity);
        void processOctree(TiXmlElement *XMLNode);
        void processLight(TiXmlElement *XMLNode, SceneNode *pParent = 0);
        void processCamera(TiXmlElement *XMLNode, SceneNode *pParent = 0);

        void processNode(TiXmlElement *XMLNode, SceneNode *pParent = 0);
        void processLookTarget(TiXmlElement *XMLNode, SceneNode *pParent);
        void processTrackTarget(TiXmlElement *XMLNode, SceneNode *pParent);
        void processEntity(TiXmlElement *XMLNode, SceneNode *pParent);
        void processSubentity(TiXmlElement *XMLNode, Entity *pEntity);
        void processParticleSystem(TiXmlElement *XMLNode, SceneNode *pParent);
        void processBillboardSet(TiXmlElement *XMLNode, SceneNode *pParent);
        void processPlane(TiXmlElement *XMLNode, SceneNode *pParent);

        void processFog(TiXmlElement *XMLNode);
        void processSkyBox(TiXmlElement *XMLNode);
        void processSkyDome(TiXmlElement *XMLNode);
        void processSkyPlane(TiXmlElement *XMLNode);
        void processClipping(TiXmlElement *XMLNode);

        void processLightRange(TiXmlElement *XMLNode, Light *pLight);
        void processLightAttenuation(TiXmlElement *XMLNode, Light *pLight);

        void processAmbientColor(TiXmlElement *XMLNode,  Ogre::SceneManager* sm);


        String getAttrib(TiXmlElement *XMLNode, const String &parameter, const String &defaultValue = "");
        Real getAttribReal(TiXmlElement *XMLNode, const String &parameter, Real defaultValue = 0);
        uint32 getAttribUint(TiXmlElement *XMLNode, const String &attrib, uint32 defaultValue = 0);
        bool getAttribBool(TiXmlElement *XMLNode, const String &parameter, bool defaultValue = false);

        Vector3 parseVector3(TiXmlElement *XMLNode);
        Quaternion parseQuaternion(TiXmlElement *XMLNode);
        ColourValue parseColour(TiXmlElement *XMLNode);

        SceneManager *mSceneMgr;
        SceneNode *mAttachNode;
        String m_sGroupName;
        String m_sPrependNode;
    };
}

#endif /* DOTXML_DOTSCENELOADER_H_ */
