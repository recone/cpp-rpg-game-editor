/*
 * DotSceneSaver.cpp
 *
 *  Created on: 11 gru 2015
 *      Author: rafal
 */

#include "DotSceneSaver.h"

using namespace std;
using namespace Ogre;


void DotSceneSaver::getMovableObjects(SceneNode* ok, int lvl,TiXmlElement *elm) {
	Ogre:SceneNode::ObjectIterator mvo = ok->getAttachedObjectIterator();
	if(mvo.hasMoreElements()) {
		while (mvo.hasMoreElements()) {
			 Ogre::MyMovableObject* obj = static_cast<MyMovableObject*>(mvo.getNext());

			 if(obj->getMovableType()=="Entity") {
				   TiXmlElement * ent = new TiXmlElement("entity");
				   ent->SetAttribute("name",obj->getName());

				   Ogre::Entity* enty = (Ogre::Entity*)obj;
				   ent->SetAttribute("meshFile",enty->getMesh()->getName());
				   ent->SetAttribute("queryflag",enty->getQueryFlags());
				   if(enty->getVisible())
					   ent->SetAttribute("visible",enty->getVisible());
			       if(enty->getListener()!=NULL) {
					   ent->SetAttribute("npc", true);
			        	}
			       ent->SetAttribute("castShadows",enty->getCastShadows());

				   int subent = enty->getNumSubEntities();
				   if(subent>0) {
					   TiXmlElement * subentities = new TiXmlElement("subentities");
					   for(int i=0;i<subent;i++) {
						   TiXmlElement * subentitie = new TiXmlElement("subentity");
							   subentitie->SetAttribute("index",i);
							   subentitie->SetAttribute("materialName",enty->getSubEntity(i)->getMaterialName());
						   subentities->LinkEndChild(subentitie);
						   }
					   ent->LinkEndChild(subentities);
					   } else {
					   if(enty->getSubEntity(0)->getMaterialName().size()!=0)
						   ent->SetAttribute("materialName",enty->getSubEntity(0)->getMaterialName());
					   }
				   elm->LinkEndChild(ent);
			 } else if(obj->getMovableType()=="Light") {
				 TiXmlElement * light = new TiXmlElement("light");
				 light->SetAttribute("name",obj->getName());
				 Light* li =(Light*)obj;
				 switch(li->getType()) {
				 	case Light::LT_POINT:
				 		light->SetAttribute("type","point");
					break;
				 	case Light::LT_DIRECTIONAL:
				 		light->SetAttribute("type","directional");
				 	break;
				 	case Light::LT_SPOTLIGHT:
				 		light->SetAttribute("type","spot");
				 	break;
				 }

				 light->SetAttribute("castShadows",li->getCastShadows());
				 TiXmlElement * diff = new TiXmlElement("colourDiffuse");
				 diff->SetAttribute("r",Ogre::StringConverter::toString(li->getDiffuseColour().r));
				 diff->SetAttribute("g",Ogre::StringConverter::toString(li->getDiffuseColour().g));
				 diff->SetAttribute("b",Ogre::StringConverter::toString(li->getDiffuseColour().b));
				 light->LinkEndChild(diff);

				 TiXmlElement * spec = new TiXmlElement("colourSpecular");
				 spec->SetAttribute("r",Ogre::StringConverter::toString(li->getSpecularColour().r));
				 spec->SetAttribute("g",Ogre::StringConverter::toString(li->getSpecularColour().g));
				 spec->SetAttribute("b",Ogre::StringConverter::toString(li->getSpecularColour().b));
				 light->LinkEndChild(spec);

				 TiXmlElement * atte = new TiXmlElement("lightAttenuation");
				 atte->SetAttribute("range",Ogre::StringConverter::toString(li->getAttenuationRange()));
				 atte->SetAttribute("constant",Ogre::StringConverter::toString(li->getAttenuationConstant()));

				 std::stringstream buff2;
 				 buff2 << std::fixed << std::setprecision(9) << li->getAttenuationLinear();

				 atte->SetAttribute("linear",buff2.str());

				 std::stringstream buff;
				 buff << std::fixed << std::setprecision(9) << li->getAttenuationQuadric();

				 atte->SetAttribute("quadratic",buff.str());
				 light->LinkEndChild(atte);


				 elm->LinkEndChild(light);
			 	 }
		}
	}
}

void DotSceneSaver::getEnvironment(TiXmlElement *elm) {
	ColourValue col = mSceneMgr->getAmbientLight();
	TiXmlElement * env = new TiXmlElement("environment");

	TiXmlElement * ambient = new TiXmlElement("colourAmbient");
	ambient->SetAttribute("r",Ogre::StringConverter::toString(col.r));
	ambient->SetAttribute("g",Ogre::StringConverter::toString(col.g));
	ambient->SetAttribute("b",Ogre::StringConverter::toString(col.b));
	ambient->SetAttribute("a",Ogre::StringConverter::toString(col.a));
	env->LinkEndChild(ambient);

	elm->LinkEndChild(env);
}


void DotSceneSaver::getNodes(SceneNode* ok, int lvl,TiXmlElement *elm) {
	Ogre::SceneNode::ChildNodeIterator it = ok->getChildIterator();
	if(it.hasMoreElements()) {
		lvl++;
		while (it.hasMoreElements() ) {
			   Ogre::MySceneNode* child = static_cast<MySceneNode*>(it.getNext());
			   if(child->getName()!="Kafle") {
				   TiXmlElement * node = new TiXmlElement("node");

				   node->SetAttribute("name",child->getName());
				   if(child->isJoint())
					   node->SetAttribute("joint",(bool)child->isJoint());
				   // create position
				   TiXmlElement * position = new TiXmlElement("position");
				   position->SetAttribute("x",Ogre::StringConverter::toString(child->getPosition().x));
				   position->SetAttribute("y",Ogre::StringConverter::toString(child->getPosition().y));
				   position->SetAttribute("z",Ogre::StringConverter::toString(child->getPosition().z));
				   node->LinkEndChild(position);

				   TiXmlElement * scale = new TiXmlElement("scale");
				   scale->SetAttribute("x",Ogre::StringConverter::toString(child->getScale().x));
				   scale->SetAttribute("y",Ogre::StringConverter::toString(child->getScale().y));
				   scale->SetAttribute("z",Ogre::StringConverter::toString(child->getScale().z));
				   node->LinkEndChild(scale);

				   TiXmlElement * rotation = new TiXmlElement("rotation");
				   rotation->SetAttribute("qx",Ogre::StringConverter::toString(child->getOrientation().x));
				   rotation->SetAttribute("qy",Ogre::StringConverter::toString(child->getOrientation().y));
				   rotation->SetAttribute("qz",Ogre::StringConverter::toString(child->getOrientation().z));
				   rotation->SetAttribute("qw",Ogre::StringConverter::toString(child->getOrientation().w));
				   node->LinkEndChild(rotation);

				   getMovableObjects(child,lvl,node);
				   getNodes(child,lvl,node);
				   elm->LinkEndChild(node);
				   }
			}
		}
}

void DotSceneSaver::saveSceneToXML() {
	TiXmlDocument doc;
	TiXmlElement * scene = new TiXmlElement("scene");
	getEnvironment(scene);
	    TiXmlElement * nodes = new TiXmlElement("nodes");
	    getNodes(mSceneMgr->getRootSceneNode(),0,nodes);
	scene->LinkEndChild(nodes);
	doc.LinkEndChild(scene);
	doc.SaveFile("Media/scene.xml");
}

/* namespace Ogre */
