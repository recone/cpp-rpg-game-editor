/*
 * DotSceneSaver.h
 *
 *  Created on: 11 gru 2015
 *      Author: rafal
 */

#ifndef SRC_DOTXML_DOTSCENESAVER_H_
#define SRC_DOTXML_DOTSCENESAVER_H_

// Includes
#include <Ogre.h>
#include <OgreString.h>
#include <OgreVector3.h>
#include <OgreQuaternion.h>

#include <list>
#include <iostream>
#include <string>
#include <iomanip>
#include <vector>

#include "tinyxml.h"

#include "../Stronicowanie/basics/Flags.h"
#include "../Stronicowanie/basics/MySceneNode.h"
#include "../Stronicowanie/basics/MyMovableObject.h"

namespace Ogre {
  class DotSceneSaver {
	public:
		DotSceneSaver(SceneManager *smg) : mSceneMgr(0) { mSceneMgr=smg; }
		virtual ~DotSceneSaver() {}

		void getNodes(SceneNode* ok, int lvl,TiXmlElement *elm);
		void getEnvironment(TiXmlElement *elm);
		void getMovableObjects(SceneNode* ok, int lvl,TiXmlElement *elm);

		void saveSceneToXML();

	private:
		SceneManager *mSceneMgr;
  };
} /* namespace Ogre */

#endif /* SRC_DOTXML_DOTSCENESAVER_H_ */
