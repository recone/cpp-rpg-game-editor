/*
 * DataConnect.h
 *
 *  Created on: Oct 8, 2015
 *      Author: rafal
 */

#ifndef SRC_STRONICOWANIE_BASICS_DATACONNECTSING_H_
#define SRC_STRONICOWANIE_BASICS_DATACONNECTSING_H_

#include "DataConnector.h"

class DataConnectSing {
public:
	static DataConnectSing* getSingleton()
	    {
		if(!da) {
			 da = new DataConnectSing();
			 da->data = new DataConnector();
			 }
		return da;
	    }

	DataConnector* getDataConnector() {
		return data;
	}


private:
	static DataConnectSing* da;
	DataConnector* data;
};


#endif /* SRC_STRONICOWANIE_BASICS_DATACONNECTSING_H_ */
