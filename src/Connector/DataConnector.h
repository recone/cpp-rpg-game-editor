/*
 * DataConnector.h
 *
 *  Created on: Oct 8, 2015
 *      Author: rafal
 */

#ifndef SRC_GUI_CONNECTOR_DATACONNECTOR_H_
#define SRC_GUI_CONNECTOR_DATACONNECTOR_H_

#include <OgreRoot.h>
#include "../DotXML/DotSceneLoader.h"
#include "../DotXML/DotSceneSaver.h"

#include "../Stronicowanie/basics/MyMovableObject.h"
#include "../Stronicowanie/Stronicowanie.h"

class DataConnector {
private:

	/* selected movable object */
	Ogre::MyMovableObject* mov = NULL;
public:
	/* free mode on/off  */
    unsigned int freeMode;
    /* draw texture/ draw terrain - size of circle */
    unsigned int painSize;
    /* draw terrain - height */
    int painHeight;

    /* scene manager */
    Ogre::SceneManager*    mSceneMgr;
	Ogre::DotSceneLoader*  scLoader;
	Ogre::DotSceneSaver*   scSaver;

	/* mouse hit point */
	Ogre::Vector3 mouseHitPos;

	/* camera */
	Ogre::Camera* mCamera;
	void setMov(Ogre::MyMovableObject* m) {
		mov = m;
	}
	void setMovNull() {
		mov = NULL;
	}
	Ogre::MyMovableObject* getMov() {
		return mov;
	}

	Stronicowanie* ground;
private:
	bool locked;

};
#endif /* SRC_GUI_CONNECTOR_DATACONNECTOR_H_ */
