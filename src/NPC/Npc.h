/*
 * Npc.h
 *
 *  Created on: 9 gru 2015
 *      Author: rafal
 */

#ifndef SRC_NPC_NPC_H_
#define SRC_NPC_NPC_H_

#include <OgreRoot.h>
#include <OgreEntity.h>
#include <OgreCamera.h>
#include <OgreTextureManager.h>
#include <OgreMaterialManager.h>

#include "../Stronicowanie/layers/Terrain.h"
#include "../Stronicowanie/basics/MyRaycast.h"
#include "../Stronicowanie/basics/MySceneNode.h"

namespace Ogre {

class Npc {
public:
	Npc();
	void startMove(Ogre::Vector3 to);
	void move(Real timePassed);
	bool isMoving();
	void movePlasetStopyer(Ogre::Real krokR);

	void setGear(int ge);

	Entity* ent;
protected:
	bool colision(Vector3 ste,Vector3 dir);
	bool angel(Vector3 pos,Vector3 dir);
	bool getGroundPosition(Ogre::Vector3 &pos);

	void stop();
	void setStop(Ogre::Vector3 to);

	int getGear();



private:
	Ogre::Real oldval = 0.0;
 	unsigned int gear = 1;
	Ogre::Vector3 kierunek = Vector3(0,0,0); // vektor kierunku
	Ogre::Vector3 cel      = Vector3(0,0,0); // cel drogi
	Ogre::Vector3 krok     = Vector3(0,0,0); // lokalny spacer na mash
	Ogre::Vector3 krok2    = Vector3(0,0,0); // globalny spacer po bitmapie

	Ogre::Real point = 0.0;
	Ogre::Real speed = 0.0;

	Ogre::Ray ray;
	bool npc_in_move = false;
};

} /* namespace Ogre */

#endif /* SRC_NPC_NPC_H_ */
