/*
 * Npc.cpp
 *
 *  Created on: 9 gru 2015
 *      Author: rafal
 */

#include "Npc.h"

namespace Ogre {
Npc::Npc():gear(5) {
}

bool Npc::angel(Vector3 pos,Vector3 dir) {
	Real suma,min,max;
	suma=0; min=0; max=0;
	Ogre::Ray locray(pos,dir);
	for(int po=0;po<ent->getBoundingBox().getHalfSize().z;po+=12) {
		  Vector3 buff=locray.getPoint(po);
		  getGroundPosition(buff);
		  suma = floor(abs(pos.y+buff.y));
		  if(po==0) {
			  max =suma;
			  min =suma;
		  	  }
		  if(max<suma) max =suma;
		  if(min>suma) min =suma;
	  	  }
	if((max-min)>40) return true;
	return false;
}

bool Npc::colision(Vector3 ste,Vector3 dir) {

	Real boxsize = ent->getBoundingBox().getHalfSize().z;

	Vector3 p1 = ray.getPoint(0);
	getGroundPosition(p1);

	Vector3 p2 = ray.getPoint(boxsize);
	getGroundPosition(p2);

	Vector3 kier=(p2-p1).normalisedCopy();

	MyRaycast* rc = new MyRaycast(1);
	rc->setRayMask(Ogre::Flags::ObjectFlags::MOVING_OBJ);
	try {
	  Vector3 cel2=ste;
	  cel2.y +=boxsize*2;
	  Ogre::Ray xc2(cel2,kier);

	  Vector3 buff;
	  Entity* ent;

	  if(rc->raycastFromPoint(xc2,buff,ent)) {
		if((xc2.getPoint(10)-buff).length()<=boxsize/2) {
		  delete rc;
		  return true;
		}
	  }
	} catch (Exception* e) {
		delete rc;
	}
	return false;
}

bool Npc::getGroundPosition(Vector3 &pos) {
	Vector3 p1 = pos;
	p1.y+=1000;
	Vector3 p2(0,-1,0);

	MyRaycast* rc = new MyRaycast(1);

	rc->setRayMask(Flags::ObjectFlags::GROUND_OBJ);
	if(rc->raycastFromPoint(p1,p2,pos)) {
		delete rc;
		return true;
		}
	delete rc;
	return false;
}

void Npc::move(Real timePassed) {
if(kierunek == Vector3(0,0,0))
	return;

  speed = 134;

  if(isMoving()) {
	 Vector3 rayPoint = ray.getPoint(point);
	 Real timeGear = (timePassed/1000)*getGear();

	if( cel != rayPoint && point>=0 ) {
	  krok = rayPoint;
	  ent->getAnimationState("Walk")->addTime(timeGear);

	  Vector3 distance = (cel - krok);
	  if(oldval> distance.length() || oldval<0) {
		  oldval = distance.length();
		  point += speed*timeGear;
	 	  getGroundPosition(krok);

	 	 if(colision(krok,kierunek)) {
	 	 		 setStop(krok);
	 			 return ;
	 	 }
 	 	 if(angel(krok,kierunek)) {
 	 		setStop(krok);
				 return ;
 	 	 }
 	 	ent->getParentSceneNode()->setPosition(krok);
        } else {
        	stop();
        }
    } else {
    	stop();
    }
  }
}

void  Npc::startMove(Vector3 to) {
	krok2 = ent->getParentSceneNode()->getPosition(); // start

	point = 0;
	cel   = to;
	kierunek=(cel-krok2).normalisedCopy();

	ray = Ray(krok2, kierunek);

	Vector3 kr = cel - Vector3(krok2.x,cel.y,krok2.z);
	ent->getParentSceneNode()->setDirection(kr,Ogre::Node::TS_WORLD,Vector3::NEGATIVE_UNIT_Z);
	ent->getAnimationState("Walk")->setEnabled(true);

	npc_in_move = true;
	oldval = -1;
	}

void Npc::stop() {
	 npc_in_move=false;
	 ent->getAnimationState("Walk")->setEnabled(false);
}

void Npc::setStop(Vector3 to) {
	 npc_in_move=false;
	 cel=to;
	 ent->getAnimationState("Walk")->setEnabled(false);
}

bool Npc::isMoving() {
	return npc_in_move;
}

void Npc::setGear(int ge) {
	gear = ge;
}
int Npc::getGear() {
	return gear;
}

} /* namespace Ogre */
