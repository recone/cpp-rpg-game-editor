/*
-----------------------------------------------------------------------------
Filename:    BaseApplication.h
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
      Tutorial Framework
      http://www.ogre3d.org/tikiwiki/
-----------------------------------------------------------------------------
*/
#ifndef __BaseApplication_h_
#define __BaseApplication_h_
// ogra
#include <OgreRoot.h>
#include <OgreConfigFile.h>

#include <OgreEntity.h>
#include <OgreLogManager.h>
#include <OgreViewport.h>
#include <OgreSceneManager.h>
#include <OgreRenderWindow.h>
#include <OgreMeshManager.h>

// IO klawiatura /myszka
#include <OISEvents.h>
#include <OISInputManager.h>
#include <OISKeyboard.h>
#include <OISMouse.h>

// kamery
#include "tray/SdkCameraMan.h"
//mygui
#include "MyGUI.h"
#include "MyGUI_OgrePlatform.h"
#include "Gui/ObjectInfo.h"
#include "Gui/LoadScreen.h"

// moje
#include "Stronicowanie/Player.h"
#include "Stronicowanie/Stronicowanie.h"
#include "Stronicowanie/basics/MyMovableObject.h"
#include "Stronicowanie/basics/MySceneNode.h"

// scene saver/loader
#include "DotXML/DotSceneLoader.h"
//mygui
#include "Gui/Interface.h"

//scena loader/saver
#include "DotXML/DotSceneLoader.h"
#include "DotXML/DotSceneSaver.h"

#include "Stronicowanie/basics/Flags.h"
#include "tray/MouseActions.h"
//moje
#include <iostream>
#include <string>

namespace Ogre
{
class BaseApplication :
	public FrameListener,
	public WindowEventListener,
	public OIS::KeyListener,
	public OIS::MouseListener,
	public MouseActions
{
private:
	//boost::shared_ptr<boost::thread> mThread;
	//boost::mutex mMutex;
	//void runThread();
public:
  BaseApplication(void);
    virtual ~BaseApplication(void);
    virtual void go(void);

protected:
  virtual bool setup();
  virtual bool configure(void);
  virtual void chooseSceneManager(void);
  virtual void createCamera(void);
  virtual void createFrameListener(void);
  virtual void destroyScene(void);
  virtual void createViewports(void);
  virtual void setupResources(void);
  virtual void createScene(void);
  virtual void createResourceListener(void);
  void createMyGUI(void);
  void createGameMap(void);

  virtual void loadResources(void);

  // Ogre::FrameListener
  virtual bool frameRenderingQueued(const Ogre::FrameEvent& evt);

  // OIS::KeyListener
  virtual bool keyPressed( const OIS::KeyEvent &arg );
  virtual bool keyReleased( const OIS::KeyEvent &arg );
  // OIS::MouseListener
  virtual bool mouseMoved( const OIS::MouseEvent &arg );
  virtual bool mousePressed( const OIS::MouseEvent &arg, OIS::MouseButtonID id );
  virtual bool mouseReleased( const OIS::MouseEvent &arg, OIS::MouseButtonID id );

  //Adjust mouse clipping area
  virtual void windowResized(Ogre::RenderWindow* rw);
  //Unattach OIS before window shutdown (very important under Linux)
  virtual void windowClosed(Ogre::RenderWindow* rw);

  //Ogre::Vector2 getMousePixel(Ogre::Real x, Ogre::Real y) ;
  Ogre::RenderWindow* mWindow;
  Ogre::String mResourcesCfg;
  Ogre::String mPluginsCfg;

  OgreBites::SdkCameraMan* mCameraMan;       // basic camera controller

  //OIS Input devices
  OIS::InputManager* mInputManager;
  OIS::Mouse*    mMouse;
  OIS::Keyboard* mKeyboard;

  Ogre::SceneNode* bigPlain;

  Stronicowanie *st;
  bool mShutDown = false;

  //Gui
  MyGUI::Gui* mGUI;
  MyGUI::OgrePlatform* mPlatform;

public:
   Ogre::Root *mRoot;

   Real oldTime = 0;
   Ogre::Timer* mTimer;

   /* main interface window */
   Gui::Interface* inter;

   /* player */
   Player* player;

   /* LoadScreen */
   LoadScreen* load;

	/* Ambient color */
	Ogre::ColourValue ambient;
};
}
#endif // #ifndef __BaseApplication_h_
