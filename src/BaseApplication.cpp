#include "BaseApplication.h"

using namespace std;
using namespace Gui;

DataConnectSing* DataConnectSing::da = NULL;

namespace Ogre {
//-------------------------------------------------------------------------------------
BaseApplication::BaseApplication(void) :mWindow(0), mResourcesCfg(Ogre::BLANKSTRING),
	mPluginsCfg(Ogre::BLANKSTRING), mCameraMan(0),  mInputManager(0), mMouse(0), mKeyboard(0), mShutDown(false)
	{ }

//-------------------------------------------------------------------------------------
BaseApplication::~BaseApplication(void){
    //if (mTrayMgr) delete mTrayMgr;
    if (mCameraMan) delete mCameraMan;
    //if (mOverlaySystem) delete mOverlaySystem;
    mGUI->shutdown();
    delete mGUI;
    mGUI = 0;
    mPlatform->shutdown();
    delete mPlatform;
    mPlatform = 0;
    Ogre::WindowEventUtilities::removeWindowEventListener(mWindow, this);
    windowClosed(mWindow);
    delete mRoot;
}

//-------------------------------------------------------------------------------------
bool BaseApplication::configure(void){
    if(mRoot->showConfigDialog()){
      mWindow = mRoot->initialise(true);
      return true;
    } else {
        return false;
    }
}

//-------------------------------------------------------------------------------------
void BaseApplication::chooseSceneManager(void) {
Ogre::LogManager::getSingletonPtr()->logMessage("*** CHOICE SCENEMANAGER ***");
mSceneMgr = mRoot->createSceneManager(Ogre::ST_GENERIC,"test");
}

//-------------------------------------------------------------------------------------
void BaseApplication::createCamera(void)
{
	Ogre::LogManager::getSingletonPtr()->logMessage("*** CREATE CMERA ***");
    // Create the camera
    mCamera = mSceneMgr->createCamera("FirstCamera");
    //Position it at 500 in Z direction
    mCamera->setPosition(Ogre::Vector3(500,500,500));
    mCamera->lookAt(Ogre::Vector3(0,0,0));
    mCamera->setNearClipDistance(25);
    mCamera->setFarClipDistance(5000);

    mCameraMan = new OgreBites::SdkCameraMan(mCamera);
}

//-------------------------------------------------------------------------------------
void BaseApplication::createScene(void)
{
	getDataConnector()->mSceneMgr = mSceneMgr;
	getDataConnector()->mCamera = mCamera;

	mSceneMgr->setShadowTechnique(SHADOWTYPE_TEXTURE_MODULATIVE);
	mSceneMgr->setShadowColour(ColourValue(0.5, 0.5, 0.5));
	mSceneMgr->setShadowTextureSize(1024*2);
	mSceneMgr->setShadowTextureCount(1);
	mSceneMgr->setShadowTextureSelfShadow(false);
	mSceneMgr->setAmbientLight(ColourValue(0.3, 0.3, 0.3, 1));
}

void BaseApplication::createMyGUI(void) {
    Ogre::LogManager::getSingletonPtr()->logMessage("*** Initializing GUI ***");
    OIS::ParamList pl;
    size_t windowHnd = 0;
    std::ostringstream windowHndStr;

    mWindow->getCustomAttribute("WINDOW", &windowHnd);
    windowHndStr << windowHnd;
    pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));

    mInputManager = OIS::InputManager::createInputSystem( pl );
    mKeyboard = static_cast<OIS::Keyboard*>(mInputManager->createInputObject( OIS::OISKeyboard, true ));
    mMouse = static_cast<OIS::Mouse*>(mInputManager->createInputObject( OIS::OISMouse, true ));

    mMouse->setEventCallback(this);
    mKeyboard->setEventCallback(this);

    //Set initial mouse clipping size
    windowResized(mWindow);

    //Register as a Window listener
    Ogre::WindowEventUtilities::addWindowEventListener(mWindow, this);
    mTimer = Ogre::Root::getSingleton().getTimer();

	mPlatform = new MyGUI::OgrePlatform();
	mPlatform->initialise(mWindow, mSceneMgr); // mWindow is Ogre::RenderWindow*, mSceneManager is Ogre::SceneManager*
	mGUI = new MyGUI::Gui();
	mGUI->initialise();

	load = new LoadScreen(mGUI);
	MyGUI::LayerManager::getInstancePtr()->resizeView(MyGUI::RenderManager::getInstancePtr()->getViewSize());
	load->show();

	mRoot->renderOneFrame();
	mWindow->update();
}
void BaseApplication::createGameMap(void) {
    /* Create big Plain */
	bigPlain=mSceneMgr->getRootSceneNode()->createChildSceneNode("Kafle");

	st = new Stronicowanie(bigPlain);
	st->setAmbient(mSceneMgr->getAmbientLight());
	st->loadTerrain("mapa.bin",1000,1000);
	st->loadLayers("terrain.png");
	st->creat3dPointer();

	getDataConnector()->ground = st;
	Ogre::DotSceneLoader* scLoader =new Ogre::DotSceneLoader();
	scLoader->parseDotScene("scene.xml","General",mSceneMgr);
	getDataConnector()->scLoader = scLoader;
	getDataConnector()->scSaver  = new Ogre::DotSceneSaver(mSceneMgr);

	/* Create player and assign to bigPlain */
	player = new Player(bigPlain);
	player->preparePlayerNode();
	player->setStartPosition(Vector3(100,0,350));
	st->dodajStrone(player->getPlayerPosition());
}

//-------------------------------------------------------------------------------------
void BaseApplication::createFrameListener(void)
{
	MyGUI::PointerManager::getInstancePtr()->setVisible(true);
	MyGUI::LayoutManager::getInstance().loadLayout("main.layout");// main layout
	MyGUI::LayoutManager::getInstance().loadLayout("menubar.layout"); // top menubar
	MyGUI::LayoutManager::getInstance().loadLayout("objectinfo.layout");  // window - object info
	MyGUI::LayoutManager::getInstance().loadLayout("lightinfo.layout");  // window - object info

	inter = new Interface(mGUI);
	inter->setStronicowanie(st); // polaczenie stronicowania z GUi - dla ShoWboxes!
	inter->setWindowWidth(mWindow->getWidth()); // ustaw szerokosc okna dla Labela FTP
	inter->setPlayer(player);
	inter->clickMenu(mGUI->findWidget<MyGUI::Button>("button4"));
	load->hide();

	mRoot->addFrameListener(this);
}
//-------------------------------------------------------------------------------------

void BaseApplication::destroyScene(void){
	//Stops the thread
	//assert(mThread);
	//mThread->join();
}

void BaseApplication::createViewports(void){
    // Create one viewport, entire window
	Ogre::LogManager::getSingletonPtr()->logMessage("*** CREATE VIEWPORT ***");
    Ogre::Viewport* vp = mWindow->addViewport(mCamera);

    Ogre::CompositorManager::getSingleton().addCompositor(mWindow->getViewport(0), "blur");
    Ogre::CompositorManager::getSingleton().setCompositorEnabled(mWindow->getViewport(0), "blur", true);
    vp->setBackgroundColour(Ogre::ColourValue(0,0,0));

    mCamera->setAspectRatio(Ogre::Real(vp->getActualWidth()) / Ogre::Real(vp->getActualHeight()));
}
//-------------------------------------------------------------------------------------
void BaseApplication::setupResources(void)
{
    // Load resource paths from config file
    Ogre::ConfigFile cf;
    cf.load(mResourcesCfg);

    // Go through all sections & settings in the file
    Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();

    Ogre::String secName, typeName, archName;
    while(seci.hasMoreElements()){
        secName = seci.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;
        for (i = settings->begin(); i != settings->end(); ++i){
            typeName = i->first;
            archName = i->second;
            Ogre::ResourceGroupManager::getSingleton().addResourceLocation(archName, typeName, secName,false,false);
        }
    }
}
//-------------------------------------------------------------------------------------
void BaseApplication::createResourceListener(void){

}
//-------------------------------------------------------------------------------------
void BaseApplication::loadResources(void){
	Ogre::LogManager::getSingletonPtr()->logMessage("*** LOAD RESORCES ***");
	Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
	Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(5);

}
//-------------------------------------------------------------------------------------
void BaseApplication::go(void){
#ifdef _DEBUG
    mResourcesCfg = "resources.cfg";
    mPluginsCfg = "plugins.cfg";
#else
    mResourcesCfg = "resources.cfg";
    mPluginsCfg = "plugins.cfg";
#endif

    if (!setup())  return;
    mRoot->startRendering();

    // clean up
    destroyScene();
}
//-------------------------------------------------------------------------------------
bool BaseApplication::setup(void)
{
    mRoot = new Ogre::Root(mPluginsCfg);
    setupResources();

    bool carryOn = configure();
    if (!carryOn) return false;

    chooseSceneManager();
    createCamera();
    createViewports();
    loadResources();

    createScene();
    createMyGUI();
    createGameMap();
    createFrameListener();

    return true;
};
//-------------------------------------------------------------------------------------
bool BaseApplication::frameRenderingQueued(const Ogre::FrameEvent& evt)
{

    if(mWindow->isClosed()) return false;
    if(mShutDown) return false;

    mGUI->injectFrameEntered(evt.timeSinceLastFrame);
    mCameraMan->frameRenderingQueued(evt);

    //Need to capture/update each device
    mKeyboard->capture();
    mMouse->capture();

    if(this->getDataConnector()->freeMode!=Ogre::Flags::SystemTryb::TRYB_WALK) {
    	st->dodajStrone(mCamera->getPosition());
    	inter->setCORD(st->get3dPointer()->getPosition());
    } else {
    	st->dodajStrone(player->getPlayerPosition());
    	inter->setCORD(player->getPlayerPosition());
    }

    if(oldTime>2) {
    	oldTime=0;
    	Ogre::RenderTarget::FrameStats stats = mWindow->getStatistics();
    	inter->setFPS("FPS: "+Ogre::StringConverter::toString(stats.avgFPS)+" Tri: "+Ogre::StringConverter::toString(stats.triangleCount));
    	}
    oldTime+=evt.timeSinceLastFrame;

    return true;
}
//-------------------------------------------------------------------------------------
bool BaseApplication::keyPressed( const OIS::KeyEvent &arg){
   // MyGUI::InputManager::getInstance().injectKeyPress(MyGUI::KeyCode::Enum(arg.key));

	OIS::MouseState stat =  mMouse->getMouseState();
	if(MyGUI::InputManager::getInstancePtr()->injectMouseMove(stat.X.abs,stat.Y.abs,stat.Z.abs)) {
	   MyGUI::InputManager::getInstancePtr()->injectKeyPress(MyGUI::KeyCode::Enum(arg.key), arg.text);
	}
    if (arg.key == OIS::KC_F){
    //toggle visibility of even rarer debugging details
    } else if (arg.key == OIS::KC_LMENU) {
    	//BaseApplication::tb1Edycja();
    	//this->tb1Edycja(mGUI->findWidget<MyGUI::Button>("tb1/edycja"));
    } else if (arg.key == OIS::KC_G) {
    	/*
    	Ogre::String prefix =Ogre::StringConverter::toString(Ogre::Root::getSingleton().getTimer()->getMilliseconds());

    	SceneNode* node = mSceneMgr->getRootSceneNode()->createChildSceneNode("ninia_node_"+prefix);
		Entity* en = mSceneMgr->createEntity("ninia_ent_"+prefix,"ninja.mesh");
			en->setCastShadows(true);

			ListenerEntity* le = new ListenerEntity(true);
			en->setListener(le);

			en->setQueryFlags(Flags::ObjectFlags::MOVING_OBJ);
			//en->setListener(this);
		node->attachObject(en);
		node->setPosition(500,0,500);
		node->scale(0.6,0.6,0.6);
		node->setInitialState();
    	 */
    } else if (arg.key == OIS::KC_Z) {
    } else if (arg.key == OIS::KC_C) {
    	this->getDataConnector()->setMovNull();
    } else if (arg.key == OIS::KC_X) {
    } else if (arg.key == OIS::KC_T) {
    } else if (arg.key == OIS::KC_H) {
    	MyGUI::MenuBar* mi = MyGUI::Gui::getInstance().findWidget<MyGUI::MenuBar>("pop");
    	bool vis = mi->getVisible();
    	mi->setVisible(!vis);

    	MyGUI::Window* wi = MyGUI::Gui::getInstance().findWidget<MyGUI::Window>("w1");
    	vis = wi->getVisible();
    	wi->setVisible(!vis);

    } else if (arg.key == OIS::KC_I) {
    	if(this->getDataConnector()->getMov()!=NULL)
    		this->getDataConnector()->getMov()->getParentSceneNode()->setPosition(st->get3dPointer()->getPosition());
    } else if (arg.key == OIS::KC_R) {
        Ogre::String newVal;
        Ogre::PolygonMode pm;

        switch (mCamera->getPolygonMode()) {
        case Ogre::PM_SOLID:
            newVal = "Wireframe";
            pm = Ogre::PM_WIREFRAME;
            break;
        case Ogre::PM_WIREFRAME:
            newVal = "Points";
            pm = Ogre::PM_POINTS;
            break;
        default:
            newVal = "Solid";
            pm = Ogre::PM_SOLID;
            break;
        }
        mCamera->setPolygonMode(pm);
    } else if (arg.key == OIS::KC_SYSRQ){
        mWindow->writeContentsToTimestampedFile("screenshot", ".jpg");
    } else if (arg.key == OIS::KC_ESCAPE){
        mShutDown = true;
    }

    if(this->getDataConnector()->freeMode>Ogre::Flags::SystemTryb::TRYB_WALK) {
    	mCameraMan->injectKeyDown(arg);
    	}
    return true;
}

bool BaseApplication::keyReleased(const OIS::KeyEvent &arg ){
	MyGUI::InputManager::getInstance().injectKeyRelease(MyGUI::KeyCode::Enum(arg.key));

	OIS::MouseState stat =  mMouse->getMouseState();
	if(MyGUI::InputManager::getInstancePtr()->injectMouseMove(stat.X.abs,stat.Y.abs,stat.Z.abs)) {
	   MyGUI::InputManager::getInstancePtr()->injectKeyRelease(MyGUI::KeyCode::Enum(arg.key));
	}

	if(this->getDataConnector()->freeMode>Ogre::Flags::SystemTryb::TRYB_WALK) {
	  mCameraMan->injectKeyUp(arg);
	}

    return true;
}

bool BaseApplication::mouseMoved(const OIS::MouseEvent &arg) {
	if(MyGUI::InputManager::getInstance().injectMouseMove(arg.state.X.abs, arg.state.Y.abs, arg.state.Z.abs)){
		return true;
		}

	std::string cursor = "arrow";
	std::string cursor2 ="";

	if (!mKeyboard->isKeyDown(OIS::KC_LCONTROL)
			&& !mKeyboard->isKeyDown(OIS::KC_Y)
				&& !mKeyboard->isKeyDown(OIS::KC_X)) {
	  if(this->getDataConnector()->freeMode>Ogre::Flags::SystemTryb::TRYB_WALK)
		mCameraMan->injectMouseMove(arg);
	  } else {
		  if(this->getDataConnector()->freeMode==Ogre::Flags::SystemTryb::TRYB_WALK)
			  mCameraMan->injectMouseMove(arg);
	  }

	if(arg.state.buttonDown(OIS::MB_Left)) {
		if(this->getDataConnector()->freeMode==Ogre::Flags::SystemTryb::TRYB_EDIT_OBJECTS) {
			if(this->getDataConnector()->getMov()==NULL)
				return false;
			Ogre::Vector3 po = this->getDataConnector()->getMov()->getJointNode()->getPosition();
			if(mKeyboard->isKeyDown(OIS::KC_X)) {
				po.x-=(this->getDataConnector()->mouseHitPos.x-arg.state.X.abs);
				this->getDataConnector()->getMov()->getJointNode()->setPosition(po);
				this->getDataConnector()->mouseHitPos.x = arg.state.X.abs;
			} else
			if(mKeyboard->isKeyDown(OIS::KC_Y)) {
				po.y+=(this->getDataConnector()->mouseHitPos.y-arg.state.Y.abs);
				this->getDataConnector()->getMov()->getJointNode()->setPosition(po);
				this->getDataConnector()->mouseHitPos.y = arg.state.Y.abs;
			}  else {
				Ogre::Vector3 hp;
				this->getMouseHitPointGround(arg,hp);
				Ogre::MyMovableObject* ob = this->getDataConnector()->getMov();
				if(ob!=NULL) {
					Vector3 tmp_vector = ob->getJointNode()->getPosition();
					tmp_vector.x =hp.x;
					tmp_vector.z =hp.z;
					ob->getJointNode()->setPosition(tmp_vector);
					}
			}
			return false;
		}
	} else {
		/**
		 * switch mouse cursor
		 */
		if(this->getDataConnector()->freeMode==Ogre::Flags::SystemTryb::TRYB_EDIT_OBJECTS) {
			Ogre::Vector3 hp2;
			if(!this->mouseOverGround(arg,hp2)) {
				cursor2="hand";
			} else {
				cursor2="arrow";

			}
		if(cursor!=cursor2) {
			cursor=cursor2;
			MyGUI::PointerManager::getInstancePtr()->setPointer(cursor2);
			}
		}
	}
}

bool BaseApplication::mousePressed( const OIS::MouseEvent &arg, OIS::MouseButtonID id )
{
	/**
	 * pass mouse position and press event to MyGUI
	 */
if(MyGUI::InputManager::getInstance().injectMousePress(arg.state.X.abs, arg.state.Y.abs,MyGUI::MouseButton::Enum(id))) {
	return true;
	}
}

bool BaseApplication::mouseReleased( const OIS::MouseEvent &arg, OIS::MouseButtonID id){
	/*
	 * pass mouse position and release event to MyGUI
	 */
   if(MyGUI::InputManager::getInstance().injectMouseRelease(arg.state.X.abs, arg.state.Y.abs,MyGUI::MouseButton::Enum(id))) {
	 return true;
   }

   Ogre::Vector3 hp;
   /**
    * walking mode
    */
   if(this->getDataConnector()->freeMode==Ogre::Flags::SystemTryb::TRYB_WALK) {
	   switch(id) {
        case OIS::MB_Left:
        	bool first = this->getMouseHitPointGround(arg,hp);
        	if(first) {
        		player->moveToPoint(hp);
				}
        break;
      }
	   /*
	    * edit mode
	    */
   } else if(this->getDataConnector()->freeMode==Ogre::Flags::SystemTryb::TRYB_EDIT_OBJECTS) {
	   bool ground = mouseOverGround(arg,hp);
	   switch(id) {
			case OIS::MB_Right:
				if(!ground && this->getDataConnector()->getMov() != NULL) {
					InfoWindow* wi = inter->selectWindow(this->getDataConnector()->getMov());
					wi->setObject(this->getDataConnector()->getMov());
					wi->showWindow();
				} else {
					this->getDataConnector()->setMov(NULL);
				}
			break;
	        case OIS::MB_Left:
				if(ground) {
					st->get3dPointer()->setPosition(hp);
				} else {
					if(getDataConnector()->getMov() != NULL)
					   getDataConnector()->getMov()->getJointNode()->showBoundingBox(false);
					try  {
						MyMovableObject* obj = this->getObjectHitObject(arg,hp);
						if(obj!=NULL) {
							getDataConnector()->setMov(obj);
							getDataConnector()->getMov()->getJointNode()->showBoundingBox(true);
						} else {
							getDataConnector()->setMov(NULL);
						}
					} catch(std::exception& e) {
						Ogre::LogManager::getSingletonPtr()->logMessage(Ogre::LML_NORMAL,e.what());
					}
				}
	        break;
	   }
	 getDataConnector()->mouseHitPos.x = arg.state.X.abs;
	 getDataConnector()->mouseHitPos.y = arg.state.Y.abs;
	 getDataConnector()->mouseHitPos.z = arg.state.Z.abs;
   } else if(this->getDataConnector()->freeMode  == Ogre::Flags::SystemTryb::TRYB_EDIT_TEXTURE
		   || this->getDataConnector()->freeMode == Ogre::Flags::SystemTryb::TRYB_EDIT_MAP) {
	 bool stan = getMouseHitPointGround(arg,hp);
   	 if(stan) {
   		Ogre::Vector2 point = st->getPixeltoVertexCoords(hp.x,hp.z);
   		st->drawCircle(point.x,point.y,this->getDataConnector()->painSize,this->getDataConnector()->painHeight);
   		st->refreshInRadius(hp.x,hp.z);
   		}
   	}

   return true;
}

//Adjust mouse clipping area
void BaseApplication::windowResized(Ogre::RenderWindow* rw)
{
    unsigned int width, height, depth;
    int left, top;
    rw->getMetrics(width, height, depth, left, top);

    const OIS::MouseState &ms = mMouse->getMouseState();
    ms.width  = width;
    ms.height = height;
}

//Unattach OIS before window shutdown (very important under Linux)
void BaseApplication::windowClosed(Ogre::RenderWindow* rw){
    //Only close for window that created OIS (the main window in these demos)
    if(rw == mWindow){
     if(mInputManager){
        mInputManager->destroyInputObject( mMouse );
        mInputManager->destroyInputObject( mKeyboard );
        OIS::InputManager::destroyInputSystem(mInputManager);
        mInputManager = 0;
        }
    }
}
}
