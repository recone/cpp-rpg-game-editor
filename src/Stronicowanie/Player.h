/*
 * Teren.h
 *
 *  Created on: 2013-03-07
 *  Author: rafal
 */

#ifndef TEREN_H_
#define TEREN_H_

#include <iostream>
#include <string>

#include <OgreEntity.h>
#include <OgreCamera.h>
#include <OgreTimer.h>
#include <OgreTextureManager.h>
#include <OgreMaterialManager.h>

#include "../Connector/DataConnect.h"

#include "basics/MyRaycast.h"
#include "basics/DynamicLines.h"


using namespace std;
using namespace Ogre;

namespace Ogre
{
class Player :
public DataConnect,public MovableObject::Listener {
public:
	Player(Ogre::SceneNode* ent);
	void preparePlayerNode();
	void setStartPosition(Vector3 pos);
	void movePlayer(Ogre::Real krokR);
	void stopPlayer();
	void stopPlayer(Ogre::Vector3 to);
	bool movePlayer();

	void moveCamera();
	void moveToPoint(Ogre::Vector3 to);
	Ogre::Entity* getPlayerEnt();
	Ogre::SceneNode* getPlayerNode();
	Ogre::Vector3 getPlayerPosition();
	bool getGroundPosition(Ogre::Vector3 &pos);
	bool colision(Vector3 ste,Vector3 dir);
	bool angel(Vector3 pos,Vector3 dir);
	void drawLine(Vector3 from,Vector3 to);
	void setGear(int ge);
	int getGear();
    /*
     * Player to Listener binding
     */
	Timer* mTimer = Ogre::Root::getSingleton().getTimer();
	unsigned long oTimer;
	unsigned long time;
    virtual bool objectRendering(const MovableObject* ent, const Camera* cam) {

		#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
				time = mTimer->getMilliseconds();
		#else
				time = mTimer->getMillisecondsCPU();
		#endif
    	movePlayer((Real)(time-oTimer)*0.001);
    	oTimer = time;
    	return true;
	  }
	~Player();

	bool move = false;
	Ogre::Image map;      //obrazek w pamieci
	Ogre::SceneNode* obj; // plain pointer bigPlain
	char* pSigned;	      // mapabitowa w vektorze
	Ogre::Real oldval = 0.0;

	Ogre::Vector3 kierunek = Vector3(0,0,0); // vektor kierunku
	Ogre::Vector3 cel      = Vector3(0,0,0); 		// cel drogi
	Ogre::Vector3 krok     = Vector3(0,0,0); 	// lokalny spacer na mash
	Ogre::Vector3 krok2    = Vector3(0,0,0); 	// globalny spacer po bitmapie
	unsigned short gear = 1 ;
	Ogre::Real point = 0.0;
	Ogre::Real speed = 0.0;
	Ogre::ManualObject* myManualObject;
	Ogre::SceneNode* myManualObjectNode;

private:
	Ogre::SceneNode* ninia; // scena playera
	Ogre::Entity* ninEnt;   // entity playera
	DynamicLines* lines;
	SceneNode* linesNode;
	Ogre::Ray ray;
   };
}
#endif /* TEREN_H_ */
