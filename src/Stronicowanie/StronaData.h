/*
 * StronaData.h
 *
 *  Created on: 1 Aug 2014
 *      Author: rafal
 */

#ifndef STRONADATA_H_
#define STRONADATA_H_

#include <OgreString.h>

namespace Ogre
{
class StronaData {
private:
	Ogre::String id; // plat id
	int posX; // position x
	int posY; // position y
public:

	Ogre::String GetId(){
		return id;
	}
	void SetId(Ogre::String id2){
		id = id2;
	}
	int GetPositionX(){
		return posX;
	}
	void SetPositionX(int pos){
		posX=pos;
	}
	int GetPositionY(){
		return posY;
	}
	void SetPositionY(int pos){
		posY=pos;
	}
};
}
#endif /* STRONADATA_H_ */
