/*
 * Teren.cpp
 *
 *  Created on: 2013-03-07
 *  Author: Rafal Lesniewski
 */

#include "Player.h"
#include <math.h>
using namespace std;

namespace Ogre
{
Player::Player(Ogre::SceneNode* ent):gear(5) {
	obj	 = ent;
}

void Player::preparePlayerNode() {
	ninia  = obj->createChildSceneNode("ninia1");
		ninEnt = getDataConnector()->mSceneMgr->createEntity("robot2","ninja.mesh");
		ninEnt->setCastShadows(true);
		ninEnt->setQueryFlags(Flags::ObjectFlags::MOVING_OBJ);
		ninEnt->setListener(this);
	ninia->attachObject(ninEnt);
	ninia->scale(0.6,0.6,0.6);
}

void Player::setStartPosition(Vector3 pos) {
	ninia->setPosition(pos);
	cel   =pos;
	krok2 =pos;
	krok  =pos;
	moveCamera();
	/*
	lines = new DynamicLines(RenderOperation::OT_LINE_LIST);
	lines->addPoint(Vector3(0,0,0));
	lines->addPoint(Vector3(0,0,0));
	lines->update();
	MovableObject* mv = (MovableObject*)lines;
	mv->setCastShadows(false);
	linesNode = getDataConnector()->mSceneMgr->getRootSceneNode()->createChildSceneNode("lines");
	linesNode->attachObject((MovableObject*)lines);
	*/
}

Entity* Player::getPlayerEnt() {
	return ninEnt;
}
Vector3 Player::getPlayerPosition() {
	return ninia->getPosition();

}
SceneNode* Player::getPlayerNode(){
	return ninia;
}

bool Player::getGroundPosition(Ogre::Vector3 &pos) {
	Vector3 p1 = pos;
	p1.y+=1000;
	Vector3 p2(0,-1,0);

	MyRaycast* rc = new MyRaycast(2);

	rc->setRayMask(Ogre::Flags::ObjectFlags::GROUND_OBJ);
	if(rc->raycastFromPoint(p1,p2,pos)) {
		return true;
		}
	delete rc;
	return false;
}

void Player::drawLine(Vector3 from,Vector3 to) {
	from.y +=50;
	lines->setPoint(0,from);
	to.y+=50;
	lines->setPoint(1,to);
	lines->update();
	linesNode->needUpdate();
}

bool Player::colision(Vector3 ste,Vector3 dir) {

	Real boxsize = ninEnt->getBoundingBox().getHalfSize().z;
	Vector3 p1 = ray.getPoint(0);
	getGroundPosition(p1);
	Vector3 p2 = ray.getPoint(boxsize);
	getGroundPosition(p2);

	Vector3 kier=(p2-p1).normalisedCopy();

	MyRaycast* rc = new MyRaycast(3);
	rc->setRayMask(Ogre::Flags::ObjectFlags::MOVING_OBJ);
	try {
	  Vector3 cel2=ste;
	  cel2.y +=boxsize*2;
	  Ogre::Ray xc2(cel2,kier);

	  Vector3 buff;
	  Entity* ent;

	  if(rc->raycastFromPoint(xc2,buff,ent) && (ent!=ninEnt)) {
		if((xc2.getPoint(10)-buff).length()<=boxsize/2) {
		  delete rc;
		  return true;
		}
	  }
	} catch (Exception* e) {
		delete rc;
	}
	return false;
}

void Player::stopPlayer() {
	 move=false;
	 ninEnt->getAnimationState("Walk")->setEnabled(false);
}

void Player::stopPlayer(Vector3 to) {
	 move=false;
	 cel=to;
	 ninEnt->getAnimationState("Walk")->setEnabled(false);
}

bool Player::angel(Vector3 pos,Vector3 dir) {
	  Real suma,min,max;
	  suma=0;
	  min=0;
	  max=0;
	  Ogre::Ray locray(pos,dir);
	  for(int po=0;po<ninEnt->getBoundingBox().getHalfSize().z;po+=12) {
		  Vector3 buff=locray.getPoint(po);
		  getGroundPosition(buff);
		  suma = floor(abs(pos.y+buff.y));
		  if(po==0) {
			  max =suma;
			  min =suma;
		  	  }
		  if(max<suma) max =suma;
		  if(min>suma) min =suma;
	  	  }
	  if((max-min)>40) return true;

	  return false;
}

bool Player::movePlayer() {
	return move;
}
void Player::setGear(int ge) {
	gear = ge;
}
int Player::getGear() {
	return gear;
}

void Player::movePlayer(Real krokR) {
if(kierunek==Vector3(0,0,0))
	return;

speed=135*krokR*getGear();
if(movePlayer()) {
	if(cel!=ray.getPoint(point) && point>=0) {
	  krok=ray.getPoint(point);
	  ninEnt->getAnimationState("Walk")->addTime(krokR*getGear());

	  if(oldval>(cel - krok).length() || oldval==NULL) {
		  oldval=(cel - krok).length();
     	  point+=speed;
	 	  getGroundPosition(krok);

	 	  if(colision(krok,kierunek)) {
	 	 		 stopPlayer(krok);
	 	 		 return ;
	 	 	 	}
 	 	  if(angel(krok,kierunek)) {
				 stopPlayer(krok);
				 return ;
 	 	  	  }

    	  ninia->setPosition(krok);
        } else {
        	stopPlayer();
        }
    } else {
    	stopPlayer();
    }
	moveCamera();
  }
}

void  Player::moveToPoint(Vector3 to) {
	krok2=ninia->getPosition();// start
	point=0;
	cel=to;

	kierunek=(cel-krok2).normalisedCopy();
	ray.setOrigin(krok2);
	ray.setDirection(kierunek);
	Vector3 kr = cel - Vector3(krok2.x,cel.y,krok2.z);
	ninia->setDirection(kr,Node::TS_WORLD,Vector3::NEGATIVE_UNIT_Z);

	ninEnt->getAnimationState("Walk")->setEnabled(true);

	move=true;
	oldval=NULL;
	}

void Player::moveCamera() {
    Vector3 npos = ninia->getPosition();
    getDataConnector()->mCamera->setPosition(npos+Vector3(1200,700,0));
    getDataConnector()->mCamera->lookAt(ninia->getPosition());
	}

Player::~Player() {
	}
}
