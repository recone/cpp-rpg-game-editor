/*
 * Raycast.h
 *
 *  Created on: 23 Aug 2014
 *      Author: rafal
 */

#ifndef RAYCAST_H_
#define RAYCAST_H_

#include <iostream>
#include <OgreRoot.h>

#include <OgreMeshManager.h>
#include <OgreEntity.h>
#include <OgreNode.h>
#include "MeshInfo.h"

#include "../layers/Terrain.h"

namespace Ogre
{
class MyRaycast : public MeshInfo {
public:
	MyRaycast();
	MyRaycast(int max);

	void setRayMask(int mask);

	bool raycastFromPoint(Ogre::Ray ray,Ogre::Vector3 &result);
	bool raycastFromPoint(const Ogre::Vector3 &point,const Ogre::Vector3 &normal,Ogre::Vector3 &result);
	bool raycastFromPoint(Ogre::Ray ray,Ogre::Vector3 &result,Ogre::Entity* &ent);

private:
	Ogre::SceneManager * getSceneManager();
	int mask = NULL;
	unsigned int maxHits = 0;
	Terrain* tr = NULL;
};
}
#endif /* RAYCAST_H_ */
