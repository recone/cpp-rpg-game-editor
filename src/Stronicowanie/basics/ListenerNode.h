/*
 * ListenerNode.h
 *
 *  Created on: 9 gru 2015
 *      Author: rafal
 */

#ifndef SRC_STRONICOWANIE_BASICS_LISTENERNODE_H_
#define SRC_STRONICOWANIE_BASICS_LISTENERNODE_H_

#include <OgreRoot.h>
#include <OgreSceneNode.h>

namespace Ogre {

class ListenerNode : public Node::Listener {
};

} /* namespace Ogre */

#endif /* SRC_STRONICOWANIE_BASICS_LISTENERNODE_H_ */
