/*
 * MyMovableObject.cpp
 *
 *  Created on: 5 wrz 2015
 *      Author: rafal
 */

#include "MyMovableObject.h"

namespace Ogre {
MySceneNode* MyMovableObject::getJointNode() {
		MySceneNode* sn = (MySceneNode*)getParentSceneNode();
		if(sn==NULL) return NULL;
		if(sn->isJoint())
			return sn;
		else
			return getJointNode((MySceneNode*)sn->getParentSceneNode());
	}
MySceneNode* MyMovableObject::getJointNode(MySceneNode* sn) {
	if(sn->isJoint())
		return sn;
	else
		return getJointNode((MySceneNode*)sn->getParentSceneNode());
	}
} /* namespace Ogre */
