/*
 * Image.cpp
 *
 *  Created on: 23 Aug 2014
 *      Author: rafal
 */

#include "MyImage.h"
namespace Ogre
{
Ogre::Image MyImage::cropImage(const Ogre::Image * source, size_t offsetX, size_t offsetY, size_t width, size_t height) {
   if(offsetX + width > source->getWidth())
      return *source;
   else if(offsetY + height > source->getHeight())
      return *source;

   size_t bpp = Ogre::PixelUtil::getNumElemBytes(source->getFormat());

   const unsigned char *srcData = source->getData();
   unsigned char *dstData = new unsigned char[width * height * bpp];

   size_t srcPitch = source->getRowSpan();
   size_t dstPitch = width * bpp;
   for(size_t col = 0; col < width * bpp; col++)
   {
	   for(size_t row = 0; row < height; row++)
      {
         dstData[(row * dstPitch) + col] = srcData[((row + offsetY) * srcPitch) + (offsetX * bpp) + col];
      }
   }

   Ogre::Image croppedImage;
   croppedImage.loadDynamicImage(dstData, width, height, 1, source->getFormat(), false);

   return croppedImage;
}
}
