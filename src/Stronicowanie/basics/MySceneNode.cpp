/*
 * MySceneNode.cpp

 *
 *  Created on: 30 lip 2015
 *      Author: rafal
 */
#include "../../BaseApplication.h"
#include "MySceneNode.h"
namespace Ogre
{

	bool MySceneNode::isJoint() {
		return joint;
	}
	void MySceneNode::setJoint(bool jt) {
		joint=jt;
	}
    void MySceneNode::destroyAllAttachedMovableObjects()	{
	   // Destroy all the attached objects
	   SceneNode::ObjectIterator itObject = this->getAttachedObjectIterator();

	   while ( itObject.hasMoreElements() )
	   {
	      MovableObject* pObject = (MovableObject*)(itObject.getNext());
	      this->getCreator()->destroyMovableObject(pObject);
	   }
	}
	void MySceneNode::setTyp(int t) {
		typ = t;
	}
	int MySceneNode::getTyp() {
		return typ;
	}
	void MySceneNode::destroyAllNodes() {
		Ogre::SceneNode::ChildNodeIterator it = this->getChildIterator();

		if(it.hasMoreElements()) {
			while (it.hasMoreElements()) {
				MySceneNode* child = (MySceneNode*)(it.getNext());
				if(child!=NULL) {
					child->destroyAllNodes();
					}
			}
		}
		this->~SceneNode();
	}
}
