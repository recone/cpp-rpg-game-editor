/*
 * MeshInfo.h
 *
 *  Created on: 23 Aug 2014
 *      Author: rafal
 */

#ifndef MESHINFO_H_
#define MESHINFO_H_

#include "OgreMesh.h"
#include "OgreSubMesh.h"


class MeshInfo {
protected:
	void getMeshInformation(const Ogre::Mesh* const mesh,size_t &vertex_count,Ogre::Vector3* &vertices,size_t &index_count,
	unsigned int* &indices,const Ogre::Vector3 &position,const Ogre::Quaternion &orient,const Ogre::Vector3 &scale);
};


#endif /* MESHINFO_H_ */
