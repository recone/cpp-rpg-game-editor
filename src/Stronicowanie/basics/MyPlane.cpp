/*
 * Plane.cpp
 *
 *  Created on: 23 Aug 2014
 *      Author: rafal
 */

#include "MyPlane.h"

namespace Ogre
{
void MyPlane::loadManualPlane(Ogre::Mesh* pMesh, MeshBuildParams& params)  {
    	Ogre::SubMesh *pSub = pMesh->createSubMesh();
        // Set up vertex data
        // Use a single shared buffer
        pMesh->sharedVertexData = OGRE_NEW Ogre::VertexData();
        Ogre::VertexData* vertexData = pMesh->sharedVertexData;
        // Set up Vertex Declaration
        Ogre::VertexDeclaration* vertexDecl = vertexData->vertexDeclaration;
        size_t currOffset = 0;
        // We always need positions
        vertexDecl->addElement(0, currOffset, Ogre::VET_FLOAT3, Ogre::VES_POSITION);
        currOffset += Ogre::VertexElement::getTypeSize(Ogre::VET_FLOAT3);

        vertexDecl->addElement(0, currOffset, Ogre::VET_COLOUR_ABGR, Ogre::VES_DIFFUSE);
        currOffset += Ogre::VertexElement::getTypeSize(Ogre::VET_COLOUR_ABGR);

        // Optional normals
        if(params.normals)
        {
            vertexDecl->addElement(0, currOffset, Ogre::VET_FLOAT3, Ogre::VES_NORMAL);
            currOffset += Ogre::VertexElement::getTypeSize(Ogre::VET_FLOAT3);
        }

        for (unsigned short i = 0; i < params.numTexCoordSets; ++i)
        {
            // Assumes 2D texture coords
            vertexDecl->addElement(0, currOffset, Ogre::VET_FLOAT2, Ogre::VES_TEXTURE_COORDINATES, i);
            currOffset += Ogre::VertexElement::getTypeSize(Ogre::VET_FLOAT2);
        }

        vertexData->vertexCount = (params.xsegments + 1) * (params.ysegments + 1);

        // Allocate vertex buffer
        Ogre::HardwareVertexBufferSharedPtr vbuf =
        	Ogre::HardwareBufferManager::getSingleton().
            createVertexBuffer(vertexDecl->getVertexSize(0), vertexData->vertexCount,
            params.vertexBufferUsage, params.vertexShadowBuffer);

        // Set up the binding (one source only)
        Ogre::VertexBufferBinding* binding = vertexData->vertexBufferBinding;
        binding->setBinding(0, vbuf);


        // Work out the transform required
        // Default orientation of plane is normal along +z, distance 0
        Ogre::Matrix4 xlate, xform, rot;
        Ogre::Matrix3 rot3;
        xlate = rot = Ogre::Matrix4::IDENTITY;
        // Determine axes
        Ogre::Vector3 zAxis, yAxis, xAxis;
        zAxis = params.plane.normal;
        zAxis.normalise();
        yAxis = params.upVector;
        yAxis.normalise();
        xAxis = yAxis.crossProduct(zAxis);
        if (xAxis.length() == 0) {
            //upVector must be wrong
            OGRE_EXCEPT(Ogre::Exception::ERR_INVALIDPARAMS, "The upVector you supplied is parallel to the plane normal, so is not valid.",
                "MeshManager::createPlane");
        }

        rot3.FromAxes(xAxis, yAxis, zAxis);
        rot = rot3;

        // Set up standard transform from origin
        xlate.setTrans(params.plane.normal * -params.plane.d);

        // concatenate
        xform = xlate * rot;

        // Generate vertex data
        // Lock the whole buffer
        float* pReal = static_cast<float*>(vbuf->lock(Ogre::HardwareBuffer::HBL_DISCARD) );
        Ogre::Real xSpace = params.width / params.xsegments;
        Ogre::Real ySpace = params.height / params.ysegments;
        Ogre::Real halfWidth = params.width / 2;
        Ogre::Real halfHeight = params.height / 2;
        Ogre::Real xTex = (1.0f * params.xTile) / params.xsegments;
        Ogre::Real yTex = (1.0f * params.yTile) / params.ysegments;
        Ogre::Vector3 vec;
        Ogre::Vector3 min = Ogre::Vector3::ZERO, max = Ogre::Vector3::UNIT_SCALE;
        Ogre::Real maxSquaredLength = 0;
        Ogre::ABGR color;
        bool firstTime = true;

        for (int y = 0; y < params.ysegments + 1; ++y)
        {
            for (int x = 0; x < params.xsegments + 1; ++x)
            {
                // Work out centered on origin
                vec.x = (x * xSpace) - halfWidth;
                vec.y = (y * ySpace) - halfHeight;
                vec.z = 0.0f;
                // Transform by orientation and distance
                vec = xform.transformAffine(vec);
                // Assign to geometry
                *pReal++ = vec.x;
                *pReal++ = vec.y;
                *pReal++ = vec.z;
                (*(Ogre::ABGR *)&(*pReal++))= Ogre::ColourValue(1.0f,1.0f,1.0f,1.0f).getAsABGR();
                // Build bounds as we go
                if (firstTime)
                {
                    min = vec;
                    max = vec;
                    maxSquaredLength = vec.squaredLength();
                    firstTime = false;
                } else {
                    min.makeFloor(vec);
                    max.makeCeil(vec);
                    maxSquaredLength = std::max(maxSquaredLength, vec.squaredLength());
                }

                if (params.normals) {
                    // Default normal is along unit Z
                    vec = Ogre::Vector3::UNIT_Z;
                    // Rotate
                    vec = rot.transformAffine(vec);

                    *pReal++ = vec.x;
                    *pReal++ = vec.y;
                    *pReal++ = vec.z;
                }

                for (unsigned short i = 0; i < params.numTexCoordSets; ++i)
                {
                    *pReal++ = x * xTex;
                    *pReal++ = 1 - (y * yTex);
                }


            } // x
        } // y

        // Unlock
        vbuf->unlock();
        // Generate face list
        pSub->useSharedVertices = true;
        tesselate2DMesh(pSub, params.xsegments + 1, params.ysegments + 1, false,
            params.indexBufferUsage, params.indexShadowBuffer);
        pMesh->_setBounds(Ogre::AxisAlignedBox(min, max), true);
        pMesh->_setBoundingSphereRadius(Ogre::Math::Sqrt(maxSquaredLength));
    }

//-----------------------------------------------------------------------
 void MyPlane::tesselate2DMesh(Ogre::SubMesh* sm, unsigned short meshWidth, unsigned short meshHeight,bool doubleSided, Ogre::HardwareBuffer::Usage indexBufferUsage, bool indexShadowBuffer)
 {
     // The mesh is built, just make a list of indexes to spit out the triangles
     unsigned short vInc, uInc, v, u, iterations;
     unsigned short vCount, uCount;

     if (doubleSided)
     {
         iterations = 2;
         vInc = 1;
         v = 0; // Start with front
     }
     else
     {
         iterations = 1;
         vInc = 1;
         v = 0;
     }

     // Allocate memory for faces
     // Num faces, width*height*2 (2 tris per square), index count is * 3 on top
     sm->indexData->indexCount = (meshWidth-1) * (meshHeight-1) * 2 * iterations * 3;
		sm->indexData->indexBuffer = Ogre::HardwareBufferManager::getSingleton().
			createIndexBuffer(Ogre::HardwareIndexBuffer::IT_16BIT,
			sm->indexData->indexCount, indexBufferUsage, indexShadowBuffer);

     unsigned short v1, v2, v3;
     //bool firstTri = true;
     Ogre::HardwareIndexBufferSharedPtr ibuf = sm->indexData->indexBuffer;
		// Lock the whole buffer
		unsigned short* pIndexes = static_cast<unsigned short*>(
			ibuf->lock(Ogre::HardwareBuffer::HBL_DISCARD) );

     while (iterations--)
     {
         // Make tris in a zigzag pattern (compatible with strips)
         u = 0;
         uInc = 1; // Start with moving +u

         vCount = meshHeight - 1;
         while (vCount--)
         {
             uCount = meshWidth - 1;
             while (uCount--)
             {
                 // First Tri in cell
                 // -----------------
                 v1 = ((v + vInc) * meshWidth) + u;
                 v2 = (v * meshWidth) + u;
                 v3 = ((v + vInc) * meshWidth) + (u + uInc);
                 // Output indexes
                 *pIndexes++ = v1;
                 *pIndexes++ = v2;
                 *pIndexes++ = v3;
                 // Second Tri in cell
                 // ------------------
                 v1 = ((v + vInc) * meshWidth) + (u + uInc);
                 v2 = (v * meshWidth) + u;
                 v3 = (v * meshWidth) + (u + uInc);
                 // Output indexes
                 *pIndexes++ = v1;
                 *pIndexes++ = v2;
                 *pIndexes++ = v3;

                 // Next column
                 u += uInc;
             }
             // Next row
             v += vInc;
             u = 0;
         }

         // Reverse vInc for double sided
         v = meshHeight - 1;
         vInc = -vInc;
     }
		// Unlock
		ibuf->unlock();
 }
 void MyPlane::createPlain(Ogre::String name){

 		pMesh = Ogre::MeshManager::getSingleton().createManual(name,Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
 		pMesh->load();
 		MeshBuildParams params;
		params.type = Ogre::Flags::MeshBuildType::MBT_PLANE;
		params.plane = mPlane;
		params.width = ramiePixel;
		params.height = ramiePixel;
		params.xsegments = ramie/3;
		params.ysegments = ramie/3;
		params.normals = true;
		params.numTexCoordSets = 1;
		params.xTile = 1;
		params.yTile = 1;
		params.upVector = Ogre::Vector3::UNIT_Z;
		params.vertexBufferUsage = Ogre::HardwareBuffer::HBU_STATIC_WRITE_ONLY;
		params.indexBufferUsage  = Ogre::HardwareBuffer::HBU_STATIC_WRITE_ONLY;
		params.vertexShadowBuffer = true;
		params.indexShadowBuffer  = true;

		loadManualPlane(pMesh.get(),params);
 }

 Plane MyPlane::getPlane() {
	 return mPlane;
 }
}
