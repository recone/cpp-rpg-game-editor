/*
 * MySceneNode.h
 *
 *  Created on: 30 lip 2015
 *      Author: rafal
 */

#ifndef SRC_STRONICOWANIE_BASICS_MYSCENENODE_H_
#define SRC_STRONICOWANIE_BASICS_MYSCENENODE_H_

#include <OgreSceneManager.h>
#include <OgreSceneNode.h>
#include <iostream>
#include "Flags.h"

using namespace std;
namespace Ogre
{
class MySceneNode : public Ogre::SceneNode {
private:
	bool joint = false;
	int typ = NULL;
public:
	MySceneNode(Ogre::SceneManager* creator) : SceneNode(creator) {	setTyp(Ogre::Flags::NodeType::UNKNOWN);}
	MySceneNode(Ogre::SceneManager* creator, const Ogre::String& name) : SceneNode(creator,name) { setTyp(Ogre::Flags::NodeType::UNKNOWN);}
	void destroyAllNodes();
	bool isJoint();
	void setJoint(bool jt);
	void destroyAllAttachedMovableObjects();
	void setTyp(int t);
	int  getTyp();
  };
}
#endif /* SRC_STRONICOWANIE_BASICS_MYSCENENODE_H_ */
