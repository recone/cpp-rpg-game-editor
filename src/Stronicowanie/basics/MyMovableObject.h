/*
 * MyMovableObject.h
 *
 *  Created on: 5 wrz 2015
 *      Author: rafal
 */

#ifndef SRC_STRONICOWANIE_BASICS_MYMOVABLEOBJECT_H_
#define SRC_STRONICOWANIE_BASICS_MYMOVABLEOBJECT_H_

#include <OgreMovableObject.h>
#include "MySceneNode.h"

#include <iostream>
#include "Flags.h"

using namespace std;
namespace Ogre
{
class MyMovableObject :	public MovableObject {
public:
	MySceneNode* getJointNode();
	MySceneNode* getJointNode(MySceneNode* sn);
  };
}
#endif  /* SRC_STRONICOWANIE_BASICS_MYMOVABLEOBJECT_H_ */
