/*
 * Test.h
 *
 *  Created on: 9 gru 2015
 *      Author: rafal
 */

#ifndef SRC_STRONICOWANIE_BASICS_LISTENERENTITY_H_
#define SRC_STRONICOWANIE_BASICS_LISTENERENTITY_H_

#include <OgreRoot.h>
#include <OgreMovableObject.h>
#include <OgreTimer.h>
#include <iostream>

#include "../../NPC/Npc.h"

using namespace std;

namespace Ogre {
class ListenerEntity:public MovableObject::Listener {
public:
	Timer* mTimer;

	unsigned long otime;
	unsigned long time;

	ListenerEntity(bool n, Entity* enti): mTimer(NULL),time(0),otime(0) {
		npc = n;

		ncc = new Npc();
		ncc->ent = enti;
		ncc->setGear(2);

		mTimer = Ogre::Root::getSingleton().getTimer();
	}

private:

	bool npc = false;
	Npc* ncc;
	bool objectRendering(const MovableObject* obj, const Camera* cam) {
		Real timePassed;
		/*
		 * Move object if right its time for it
		 */

		if(npc) {
			// ncc->ent = (Entity*)obj;

			#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
				time   = mTimer->getMilliseconds();
			#else
				time   = mTimer->getMillisecondsCPU();
			#endif

			if(!ncc->isMoving()) {

				float numb  = ((double) rand() / (RAND_MAX/2)) - 1;
				float numb2 = ((double) rand() / (RAND_MAX/2)) - 1;

				// generate new direction
				Vector3 pos =obj->getParentSceneNode()->getInitialPosition();
				ncc->startMove(Vector3(pos.x+(700*numb),0,pos.z+(700*numb2)));

				otime = time;
			} else {
				timePassed = (Real)(time-otime);
				if(timePassed > 30) {
					ncc->move(timePassed);
					otime = time;
				}
			}
		}

		return true;
	}
};

} /* namespace Ogre */

#endif /* SRC_STRONICOWANIE_BASICS_LISTENERENTITY_H_ */
