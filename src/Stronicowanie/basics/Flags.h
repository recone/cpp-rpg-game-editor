/*
 * QueryFlags.h
 *
 *  Created on: 28 paź 2015
 *      Author: rafal
 */

#ifndef SRC_STRONICOWANIE_BASICS_FLAGS_H_
#define SRC_STRONICOWANIE_BASICS_FLAGS_H_

namespace Ogre {
	class Flags {
		public:
		enum ObjectFlags
		{
		  GROUND_OBJ =1<<2, // 1<<1,
		  MOVING_OBJ =1<<1, // 1<<0,
		  STATIC_OBJ =1<<3 // 1<<2
		};
		enum NodeType
		{
		  UNKNOWN = 1,
		  OMNI_LIGHT = 2
		};
		enum SystemTryb
		{
		  TRYB_WALK = 1,
		  TRYB_EDIT_TEXTURE = 2,
		  TRYB_EDIT_MAP = 3,
		  TRYB_EDIT_OBJECTS = 4
		};

	    enum MeshBuildType
	    {
	      MBT_PLANE,
	      MBT_CURVED_ILLUSION_PLANE,
	      MBT_CURVED_PLANE
	    };
	};
} /* namespace Ogre */

#endif /* SRC_STRONICOWANIE_BASICS_FLAGS_H_ */
