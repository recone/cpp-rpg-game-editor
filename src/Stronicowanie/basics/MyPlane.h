/*
 * Plane.h
 *
 *  Created on: 23 Aug 2014
 *      Author: rafal
 */

#ifndef PLANE_H_
#define PLANE_H_

#include <OgreRoot.h>
#include <OgreMeshManager.h>

#include "OgreMesh.h"
#include "OgreSubMesh.h"
#include "OgreHardwareBuffer.h"
#include "OgreHardwareBufferManager.h"
#include "Flags.h"

namespace Ogre
{
class MyPlane {
protected:
 	Ogre::MeshPtr pMesh;

    struct MeshBuildParams {
 		Flags::MeshBuildType type;
        Ogre::Plane plane;
        Ogre::Real width;
        Ogre::Real height;
        Ogre::Real curvature;
        int xsegments;
        int ysegments;
        bool normals;
        unsigned short numTexCoordSets;
        Ogre::Real xTile;
        Ogre::Real yTile;
        Ogre::Vector3 upVector;
        Ogre::Quaternion orientation;
        Ogre::HardwareBuffer::Usage vertexBufferUsage;
        Ogre::HardwareBuffer::Usage indexBufferUsage;
        bool vertexShadowBuffer;
        bool indexShadowBuffer;
        int  ySegmentsToKeep;
    };

	Ogre::Plane mPlane;
	 /*
	  * Ile elementow na ramie placka?
	  */
	/*
	 * How many batches per plane
	 */
    char ramie;
    /*
     * Plane size
     */
    int ramiePixel;
	void tesselate2DMesh(Ogre::SubMesh* sm, unsigned short meshWidth, unsigned short meshHeight,bool doubleSided, Ogre::HardwareBuffer::Usage indexBufferUsage, bool indexShadowBuffer);
	void loadManualPlane(Ogre::Mesh* pMesh,MeshBuildParams& params);
	void createPlain(Ogre::String name);

public:
	Ogre::Plane getPlane();
};
}

#endif /* PLANE_H_ */
