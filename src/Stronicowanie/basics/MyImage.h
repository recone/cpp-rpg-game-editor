/*
 * Image.h
 *
 *  Created on: 23 Aug 2014
 *      Author: rafal
 */

#ifndef IMAGE_H_
#define IMAGE_H_

#include <OgreImage.h>
namespace Ogre
{
class MyImage {
protected:
    Image cropImage(const Image * source, size_t offsetX, size_t offsetY, size_t width, size_t height);
};
}
#endif /* IMAGE_H_ */
