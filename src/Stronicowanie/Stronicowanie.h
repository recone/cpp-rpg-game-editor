/*
 * Stronicowanie.h
 *
 *  Created on: 14 paź 2013
 *      Author: rafal
 */

#ifndef STRONICOWANIE_H_
#define STRONICOWANIE_H_

#include <OgreEntity.h>
#include <OgreMeshManager.h>
#include <OgreLogManager.h>

#include "basics/Flags.h"

#include "layers/Terrain.h"
#include "layers/LayersTextures.h"
#include "Strona.h"

using namespace std;
namespace Ogre {
class Stronicowanie {
public:
	Stronicowanie(SceneNode* bi);
	~Stronicowanie();
	bool dodajStrone(Vector3 po); // tworzy 9 nowych plackow jesli pozycja playera sie zmienila
	Plane getFirstPlane();
	bool findPlaneOnList(int x,int y); // plane in pos x y exists?
	Real getDistanceToPlayerPos(int x, int z); // liczy odleglosc placka od playera
	int* getPlaneXY(int x,int y); // array of elements is returned
	Strona* getPlaneFromList(int x,int y);
	Vector2 getPixeltoVertexCoords(Real x, Real y);
	void removeOldPlanes();
	void removeAllPlanes();
	void refreshAllPlanes();
	void refreshInRadius(int x,int y);// odswierz placki w promieniu od punktu x,y
	void refreshInRadius(int x,int y,bool terrain);
	void setAllBoundinBoxes(bool show); // pokaz/ukryj wszystkie bounding boxy
	void creat3dPointer();
	SceneNode* get3dPointer();
	Terrain* getTerrainPtr();
	LayersTextures* getLayersTexturesPtr();

	void loadTerrain(string name,int width,int height);
	void loadLayers(string name);

	void drawCircle(long x0, long y0, long radius,int height);

	SceneNode* bigPlain; // duzy placek rodzic - niewidoczny
	std::list<Strona*> lista; // lista plackow

	Real last_x = 0.0;
	Real last_y = 0.0;
	unsigned short radius = 1; //ile plackow rysowac? / promien

	bool showBoxes = false;	   //pokaz boxy ?

	void setSelectedLayer(int lid);
	int getSelectedLayer();

	void setAmbient(ColourValue am);
	ColourValue getAmbient();
private:
   void drawCirclePixel(int x, int y,char step,char max);
   String getNamePref(int x,int y); //get string name for plane/entity
   Image im; // circle image
   const int planeWeight;
   const int planeElements;
   Terrain* terrain;
   LayersTextures* layers;
   unsigned short layerId = 0; // id of selected layer

   SceneNode* sphareNode;
   ColourValue ambient;
  };
}
#endif /* STRONICOWANIE_H_ */
