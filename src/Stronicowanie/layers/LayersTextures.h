/*
 * Layers.h
 *
 *  Created on: 15 Sep 2014
 *      Author: rafal
 */

#ifndef LAYERS_H_
#define LAYERS_H_

#include <iostream>
#include <OgreImage.h>
#include "../basics/MyImage.h"

using namespace std;
using namespace Ogre;
/*
 * Texture 1
 * R - diffuse
 * G - texture 1
 * B - texture 2
 */
class LayersTextures :
public MyImage {
	public:
		LayersTextures(string map);
		Image getCropImage(size_t offsetX, size_t offsetY, size_t width, size_t height);
		Image getCropImage(const Image * source, size_t offsetX, size_t offsetY, size_t width, size_t height);
		void save();
	public:
		string name;
		Image map; // bitmap with layers
	  };
#endif /* LAYERS_H_ */
