/*
 * Terrain.h
 *
 *  Created on: Sep 30, 2015
 *      Author: rafal
 */

#ifndef SRC_STRONICOWANIE_LAYERS_TERRAIN_H_
#define SRC_STRONICOWANIE_LAYERS_TERRAIN_H_

#include <OgreResourceGroupManager.h>
#include <iostream>

using namespace std;
using namespace Ogre;

class Terrain {
 public:
	Terrain();
	Terrain(string name,long w=1000,long h=1000);
	short* loadTerrainData(string name,long w=1000,long h=1000); // load terrain map
	void saveTerrainData();
	Terrain* getTerrainPtr();
	short* getTerrainDataPtr();
	short getPixel(long x,long y); // get terrain stat on X,Y
	void setPixel(long x,long y,short val);
	int  getHeight();
	int  getWidth();
 private:
	void setMapSize(long w,long h); // ser map size

	short* terrain; // raw bitmap XxY
	long h;// height of a terain map -2d
	long w;// weight of a terrain map -2d
	string name;
	DataStreamPtr pStream;
 };
#endif /* SRC_STRONICOWANIE_LAYERS_TERRAIN_H_ */
