/*
 * Terrain.cpp
 *
 *  Created on: Sep 30, 2015
 *      Author: rafal
 */

#include "Terrain.h"

Terrain::Terrain() {
	}

Terrain::Terrain(string name,long w,long h) {
	loadTerrainData(name,w,h);
	}

Terrain* Terrain::getTerrainPtr() {
	return this;
	}
short* Terrain::loadTerrainData(string name,long w, long h) {
	setMapSize(w,h);
	this->name = name;
	pStream = ResourceGroupManager::getSingleton().openResource(this->name,"General");
	pStream.get()->read(getTerrainDataPtr(),getHeight()*getWidth()*sizeof(short));
	pStream.get()->close();

	return getTerrainDataPtr();
	}
void Terrain::saveTerrainData() {
	pStream = ResourceGroupManager::getSingleton().createResource(this->name,"General",true);
	pStream.get()->write(this->terrain,getHeight()*getWidth()*sizeof(short));
	pStream.get()->close();
}

void Terrain::setMapSize(long w, long h) {
	this->w =w;
	this->h =h;
	this->terrain = new  short [w*h];
	}

int Terrain::getHeight() {
	return this->h;
	}

int Terrain::getWidth() {
	return this->w;
	}

short* Terrain::getTerrainDataPtr() {
	return this->terrain;
	}

short Terrain::getPixel(long x,long y) {
	if(y<getHeight() && x<getWidth())
	return this->terrain[y*this->w+x];
	}

void Terrain::setPixel(long x,long y,short val) {
	if(y<getHeight() && x<getWidth())
		this->terrain[y*this->w+x] = val;
	}
