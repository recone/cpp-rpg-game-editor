/*
 * Layers.cpp
 *
 *  Created on: 15 Sep 2014
 *      Author: rafal
 */

#include "LayersTextures.h"

LayersTextures::LayersTextures(string map) {
	name=map;
	this->map.load(map,"General");
}
Image LayersTextures::getCropImage(size_t offsetX, size_t offsetY, size_t width, size_t height) {
	return cropImage(&this->map,offsetX,offsetY,width,height);
}
Image LayersTextures::getCropImage(const Image * source, size_t offsetX, size_t offsetY, size_t width, size_t height) {
	return cropImage(source,offsetX,offsetY,width,height);
}
void LayersTextures::save() {
	this->map.save("Media/"+name);
}
