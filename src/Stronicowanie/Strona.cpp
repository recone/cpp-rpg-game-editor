/*
 * Strona.cpp
 *
 *  Created on: 14 paź 2013
 *      Author: rafal
 */
#include "Strona.h"
#include "../Connector/DataConnectSing.h"

using namespace std;

namespace Ogre {

//int Strona::global; // definujemy statyczna zmienna
Strona::Strona(SceneNode* bi,Terrain* terrain,LayersTextures* layers,bool bShow):p0(0),xLocalPos(0),yLocalPos(0) {
	showBox  = bShow;
	bigPlain = bi;

	this->terrain = terrain;
	this->layers  = layers;
}


void Strona::refreshMaterial() {
try {
	bool assign = false;
    if(mat.isNull()) {
		mat = MaterialManager::getSingleton().create("Mat"+ob->GetId(),ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME );
	} else {
		mat->unload();
		mat->getTechnique(0)->getPass(0)->removeAllTextureUnitStates();
		mat->removeAllTechniques();
		Ogre::MaterialManager::getSingleton().remove(mat->getHandle());
		mat.getPointer()->unload();
		Ogre::TextureManager::getSingleton().remove("AlphaBlendTexture"+ob->GetId());
		mat.setNull();
		mat = Ogre::MaterialManager::getSingleton().create("Mat"+ob->GetId(),ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
		assign=true;
	}

    mat->getTechnique(0)->getPass(0)->setVertexProgram("TerrainVP");
    mat->getTechnique(0)->getPass(0)->setFragmentProgram("TerrainFP");
    Ogre::GpuProgramParametersSharedPtr pParams = mat->getTechnique(0)->getPass(0)->getFragmentProgramParameters();

    pParams->setNamedConstant("alpha", 0);
    pParams->setNamedConstant("texture0", 1);
    pParams->setNamedConstant("texture1", 2);
    pParams->setNamedConstant("texture2", 3);

    pParams->setNamedAutoConstant("ambient",Ogre::GpuProgramParameters::AutoConstantType::ACT_AMBIENT_LIGHT_COLOUR);

    Ogre::Image im = layers->getCropImage(ob->GetPositionX(),ob->GetPositionY(),ramie,ramie);
    Ogre::TextureUnitState* tus = mat->getTechnique(0)->getPass(0)->createTextureUnitState();
    tus->setTextureAddressingMode(Ogre::TextureUnitState::TAM_CLAMP);
	tus->setTexture(
		Ogre::TextureManager::getSingleton().loadImage("AlphaBlendTexture"+ob->GetId(),
		Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,im)
	  );
	tus->setTextureRotate(Ogre::Radian(3.1415));

	tus = mat->getTechnique(0)->getPass(0)->createTextureUnitState("Dirt.jpg");
	tus->setTextureScale(0.2,0.2);

	tus = mat->getTechnique(0)->getPass(0)->createTextureUnitState("terr_rock6.jpg");
	tus->setTextureScale(0.25,0.25);

	tus  = mat->getTechnique(0)->getPass(0)->createTextureUnitState("grass_1024.jpg");
	tus->setTextureScale(0.25,0.25);

	mat->getTechnique(0)->createPass();
	mat->getTechnique(0)->getPass(1)->setAmbient(this->getAmbient());
	mat->getTechnique(0)->getPass(1)->setSceneBlending(SceneBlendType::SBT_MODULATE);
	mat->getTechnique(0)->getPass(1)->setDiffuse(1,1,1,1);
	im.freeMemory();

	/*
	mat->getTechnique(0)->getPass(1)->setSceneBlending(SceneBlendType::SBT_ADD);
    mat->getTechnique(0)->getPass(1)->setVertexProgram("LightVP");
    mat->getTechnique(0)->getPass(1)->setFragmentProgram("LightFP");
	*/

    if(assign)
	   p0->setMaterial(mat);

    p0->setCastShadows(false);
} catch (Ogre::Exception& e) {
	Ogre::LogManager::getSingletonPtr()->logMessage(Ogre::LML_NORMAL,"Exception: Terrain - material0");
	Ogre::LogManager::getSingletonPtr()->logMessage(Ogre::LML_NORMAL,e.getFullDescription());
  }
}

/*
 * Refresh height map!
 */
void Strona::refresh() {

	if(ob->GetPositionX()<0 || ob->GetPositionY()<0 ||
    		ob->GetPositionX()>(terrain->getWidth()-1) || ob->GetPositionY()>(terrain->getHeight()-1)) {
    		return ;
    	}

	float maxHei=0;
	float minHei=0;
	try {
		Ogre::SubMesh* submesh;
			Ogre::MeshPtr mesh= p0->getMesh();
			for(int i = 0;i < mesh->getNumSubMeshes();i++) {
				submesh = mesh->getSubMesh(i);
				}
			Ogre::VertexData* vertex_data = submesh->useSharedVertices ? mesh->sharedVertexData : submesh->vertexData;

			const Ogre::VertexElement* posElem = vertex_data->vertexDeclaration->findElementBySemantic(Ogre::VES_POSITION);
			const Ogre::VertexElement* posNormal = vertex_data->vertexDeclaration->findElementBySemantic(Ogre::VES_NORMAL);
			Ogre::HardwareVertexBufferSharedPtr vbuf = vertex_data->vertexBufferBinding->getBuffer(posElem->getSource());

			unsigned char* vertex = static_cast<unsigned char*>(vbuf->lock(Ogre::HardwareBuffer::HBL_NORMAL));
			Ogre::Real* pReal;
			Ogre::Real* pReal3;

			for(size_t j = 0; j < vertex_data->vertexCount; j++, vertex += vbuf->getVertexSize())  {
				posElem->baseVertexPointerToElement(vertex, &pReal);
				posNormal->baseVertexPointerToElement(vertex, &pReal3);
				int x = ob->GetPositionX()+(((ramiePixel/2)+pReal[0])/ramiePixel)*ramie;
				int y = ob->GetPositionY()+(((ramiePixel/2)+pReal[2])/ramiePixel)*ramie;

				if(x<terrain->getWidth() && y<terrain->getHeight()) {
					pReal[1]=(float)terrain->getPixel(x,y);
					if(pReal[1]>maxHei) maxHei=pReal[1];
					if(pReal[1]<minHei) minHei=pReal[1];
					}
			   }
			vbuf->unlock();

		    if(maxHei==0 && minHei==0) {
		    	maxHei=2;
		    }
		    p0->getMesh()->_setBounds(Ogre::AxisAlignedBox((ramiePixel/2)*(-1),minHei,(ramiePixel/2)*(-1),(ramiePixel/2),maxHei,(ramiePixel/2)));
		    cn0->needUpdate();
		    if(showBox)
		    	  cn0->showBoundingBox(true);
		    	else
		    	  cn0->showBoundingBox(false);
		    cn0->_updateBounds();
	} catch (Ogre::Exception& e) {
		Ogre::LogManager::getSingletonPtr()->logMessage(Ogre::LML_NORMAL,"Exception: Add strona main-loop");
		Ogre::LogManager::getSingletonPtr()->logMessage(Ogre::LML_NORMAL,e.getFullDescription());
	}
}

StronaData * Strona::createId(int x,int y) {
    int locH = terrain->getHeight() / 2;
    int locW = terrain->getWidth() / 2;

    int by=(y*ramie)+locH;
    int bx=(x*ramie)+locW;

	ostringstream byStr; //lokalizacja placka String
	ostringstream bxStr; //lokalizacja placka String
	bxStr << by;
	byStr << bx;

	mPlane.normal = Ogre::Vector3::UNIT_Y;
	mPlane.d = 0;

	StronaData * elm = new StronaData();
	elm->SetId("x:"+bxStr.str()+"y:"+byStr.str());
	elm->SetPositionX(bx);
	elm->SetPositionY(by);

	return elm;
}

int Strona::add(int x,int y) {
	xLocalPos=x;
	yLocalPos=y;

    ob = createId(x,y);

    if(ob->GetPositionX()<0 || ob->GetPositionY()<0 ||
    		ob->GetPositionX()>(terrain->getWidth()-1) || ob->GetPositionY()>(terrain->getHeight()-1)) {
    		return -1;
    	}
try {
    createPlain("ground"+ob->GetId());
	try {
		cn0 = bigPlain->createChildSceneNode("gr"+ob->GetId(),Ogre::Vector3::UNIT_Y);
	} catch (Ogre::Exception& e) {
		cn0 = DataConnectSing::getSingleton()->getDataConnector()->mSceneMgr->getSceneNode("gr"+ob->GetId());
	}
	try {
		// create entity from new plain
		p0 = DataConnectSing::getSingleton()->getDataConnector()->mSceneMgr->createEntity("GroundEntity"+ob->GetId(),"ground"+ob->GetId());
		p0->setQueryFlags(Flags::ObjectFlags::GROUND_OBJ);
		p0->setCastShadows(false);

	} catch (Ogre::Exception& e) {
		p0 = DataConnectSing::getSingleton()->getDataConnector()->mSceneMgr->getEntity("GroundEntity"+ob->GetId());
	}
	cn0->setPosition(((x*ramiePixel)+(ramiePixel/2)),0,((y*ramiePixel)+(ramiePixel/2)));

    this->refresh();
	this->refreshMaterial();

	//p0->getMesh()->setAutoBuildEdgeLists(true);
    p0->getMesh()->buildEdgeList();
	//p0->getMesh()->prepareForShadowVolume();

    try {
    	if(!mat.isNull())
    	  p0->setMaterial(mat);

		cn0->attachObject(p0);
    } catch (Ogre::Exception& e) {
    	cn0->attachObject(p0);
    }
} catch (Ogre::Exception& e) {
	Ogre::LogManager::getSingletonPtr()->logMessage(Ogre::LML_NORMAL,"Exception: Add/refresh");
	Ogre::LogManager::getSingletonPtr()->logMessage(Ogre::LML_NORMAL,e.getFullDescription());
	return -1;
}
return 1;
}


void Strona::setBoundingBox(bool show) {
	showBox= show;
    if(showBox)
	  cn0->showBoundingBox(true);
	else
	  cn0->showBoundingBox(false);
}


void Strona::setAmbient(Ogre::ColourValue am) {
	this->ambient = am;
}
Ogre::ColourValue Strona::getAmbient() {
	return this->ambient;
}

/*
 * Pass block size
 * @blockSize - size in pixels
 * @nrbatch - number of batches per block/lane
 */
void Strona::setPlainSize(int blockSize,int nrbatch) {
	ramiePixel = blockSize;
    ramie = nrbatch;
}

Strona::~Strona() {
	p0->setVisible(false);
	cn0->setVisible(false,false);

	if(!mat.isNull()) {
		mat->unload();
		mat->getTechnique(0)->getPass(0)->removeAllTextureUnitStates();
		mat->removeAllTechniques();
		//Ogre::MaterialManager::getSingleton().remove(mat->getName());
		Ogre::MaterialManager::getSingleton().remove(mat->getHandle());
		mat.getPointer()->unload();
		Ogre::TextureManager::getSingleton().remove("AlphaBlendTexture"+ob->GetId());
		}
	Ogre::MeshManager* me = Ogre::MeshManager::getSingletonPtr();
	//me->destroyResourcePool("ground"+ob->GetId());

	me->unload(pMesh->getName());
	me->remove(pMesh->getName());
	DataConnectSing::getSingleton()->getDataConnector()->mSceneMgr->destroyManualObject(pMesh->getName());
	DataConnectSing::getSingleton()->getDataConnector()->mSceneMgr->destroyEntity(p0);
	cn0->removeAndDestroyAllChildren();

	DataConnectSing::getSingleton()->getDataConnector()->mSceneMgr->destroySceneNode(cn0);
	delete ob;
   }
}
