/*
 * Strona.h
 *
 *  Created on: 14 paź 2013
 *      Author: rafal
 */

#ifndef STRONA_H_
#define STRONA_H_

#include <iostream>
#include <string>

#include <OgreTechnique.h>
#include <OgreTexture.h>
#include <OgreTextureManager.h>
#include <OgreMaterialManager.h>

#include <OgreSceneNode.h>
#include <OgreLogManager.h>

#include "basics/MyPlane.h"
#include "layers/Terrain.h"
#include "layers/LayersTextures.h"
#include "StronaData.h"


namespace Ogre
{
class Strona :
	public MyPlane {

public:
	Strona(Ogre::SceneNode* bi,Terrain* terrain,LayersTextures* layers,bool bShow);
	virtual ~Strona();
	int add(int x,int y); // create page

	void refresh(); // refresh mesh/bounding/material and so on
	void refreshMaterial();
	void setBoundingBox(bool show); // pokazac puszke?
	void setPlainSize(int width,int vertex);
	void setAmbient(Ogre::ColourValue am);
	Ogre::ColourValue getAmbient();

	Ogre::SceneNode* bigPlain;
    Ogre::SceneNode* cn0; //lokalna scena - ten placek
    Ogre::Entity* p0; //plane entyti

    int xLocalPos;
    int yLocalPos;

    StronaData * ob; // dane strony

    bool showBox = false;
private:
	StronaData * createId(int x,int y);
    Ogre::MaterialPtr mat; ///material plain

    Terrain* terrain; // DataTerrain pointer
    LayersTextures* layers; //DataLayers pointer
    Ogre::ColourValue ambient; //ambient color
  };
}
#endif /* STRONA_H_ */
