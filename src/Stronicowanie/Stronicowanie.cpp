/*
 * Stronicowanie.cpp
 *
 *  Created on: 14 paź 2013
 *      Author: rafal
 */

#include "Stronicowanie.h"
#include "../Connector/DataConnectSing.h"

namespace Ogre {
Stronicowanie::Stronicowanie(Ogre::SceneNode* bi) : radius(5),last_y(1),last_x(1),planeWeight(1000),planeElements(50) {
	bigPlain =bi;
    }

void Stronicowanie::setSelectedLayer(int lid) {
	this->layerId=lid;
	}

int Stronicowanie::getSelectedLayer() {
	return layerId;
	}

void Stronicowanie::loadTerrain(string name,int width,int height) {
	this->terrain = new Terrain(name,width,height);
	}

void Stronicowanie::loadLayers(string name) {
	this->layers = new LayersTextures(name);
	}

Terrain* Stronicowanie::getTerrainPtr() {
	return this->terrain;
    }

LayersTextures*	Stronicowanie::getLayersTexturesPtr() {
	return this->layers;
	}

void  Stronicowanie::setAllBoundinBoxes(bool show) {
	showBoxes = show;
	for (list<Strona*>::iterator it = lista.begin(); it != lista.end(); it++) {
		Strona* bufor =it.operator *();
		bufor->setBoundingBox(show);
	}
}
bool Stronicowanie::findPlaneOnList(int x,int y) {
	for (list<Strona*>::iterator it = lista.begin(); it != lista.end(); it++) {
		Strona* bufor=it.operator *();
		if(bufor->xLocalPos==x && bufor->yLocalPos==y)
				return true;
		}
	return false;
}

Strona* Stronicowanie::getPlaneFromList(int x,int y) {
	for (list<Strona*>::iterator it = lista.begin(); it != lista.end(); it++) {
		Strona* bufor =it.operator *();
		if(bufor->xLocalPos==x && bufor->yLocalPos==y)
				return bufor;
	}
}

Ogre::Real Stronicowanie::getDistanceToPlayerPos(int x, int z) {
	Ogre::Vector2 pos1 = Ogre::Vector2(x,z);
	Ogre::Vector2 pos2 = Ogre::Vector2(last_x,last_y);
	return (pos2-pos1).length();
}

void Stronicowanie::removeAllPlanes() {
Strona* bufor;
while(lista.size()>0) {
	for (list<Strona*>::iterator it = lista.begin(); it != lista.end(); it++) {
		bufor=it.operator *();
		delete bufor;
		it =lista.erase(it);
		}
	}
}

void Stronicowanie::refreshAllPlanes() {
	for (list<Strona*>::iterator it = lista.begin(); it != lista.end(); it++) {
		Strona* bufor =it.operator *();
		bufor->refresh();
		}
}

void Stronicowanie::removeOldPlanes() {
	Ogre::Real dist=0;
	for (list<Strona*>::iterator it = lista.begin(); it != lista.end(); it++) {
		Strona* bufor = it.operator *();
			dist=this->getDistanceToPlayerPos(bufor->xLocalPos,bufor->yLocalPos);
			if(abs(dist)>radius*2) {
				delete bufor;
				it =lista.erase(it);
				}
		}
}

void Stronicowanie::refreshInRadius(int x,int y) {
	try {
		int * elms = this->getPlaneXY(x,y);
		int buf_x=elms[0];
		int buf_y=elms[1];
		delete[] elms;
		int rad = round(radius/3.5);

		for(int ix=buf_x-rad;ix<=buf_x+rad;ix++) {
			for(int iy=buf_y-rad;iy<=buf_y+rad;iy++){
				if(this->findPlaneOnList(ix,iy)){
					Strona* bufor =this->getPlaneFromList(ix,iy);
					if(getSelectedLayer()==0)
						bufor->refresh();
					else
						bufor->refreshMaterial();
				}
			}
		}
	} catch (Ogre::Exception& e) {
		Ogre::LogManager::getSingletonPtr()->logMessage(Ogre::LML_NORMAL,e.getFullDescription());
	}
}

void Stronicowanie::refreshInRadius(int x,int y,bool terrain) {
	try {
		int * elms = this->getPlaneXY(x,y);
		int buf_x=elms[0];
		int buf_y=elms[1];
		delete[] elms;
		int rad = 2;

		for(int ix=buf_x-rad;ix<buf_x+rad;ix++) {
			for(int iy=buf_y-rad;iy<buf_y+rad;iy++){
				if(this->findPlaneOnList(ix,iy)){
					Strona* bufor =this->getPlaneFromList(ix,iy);
					if(terrain)
						bufor->refresh();
					else
						bufor->refreshMaterial();
				}
			}
		}
	} catch (Ogre::Exception& e) {
		Ogre::LogManager::getSingletonPtr()->logMessage(Ogre::LML_NORMAL,e.getFullDescription());
	}
}

bool Stronicowanie::dodajStrone(Ogre::Vector3 po) {
	int * elms = this->getPlaneXY(po.x,po.z);
	int buf_x=elms[0];
	int buf_y=elms[1];
	delete[] elms;

	Ogre::Real dist=0;
	int stat = 0;
	try {
		if(last_x!=buf_x || last_y!=buf_y) { // jesli nadepnieto nowy placek
			last_y=buf_y;last_x=buf_x;
			for(int ix=buf_x-radius;ix<=buf_x+radius;ix++) {
				for(int iy=buf_y-radius;iy<=buf_y+radius;iy++){
					dist=this->getDistanceToPlayerPos(ix,iy);
					if(this->findPlaneOnList(ix,iy)==false && abs(dist)<radius) {
						Strona* nw=new Strona(bigPlain,this->getTerrainPtr(),this->getLayersTexturesPtr(),showBoxes);
						//nw->setDataConnector(getDataConnector());
						nw->setAmbient(getAmbient());
						nw->setPlainSize(planeWeight,planeElements);
						stat=nw->add(ix,iy);
						if(stat>=0)
							lista.push_back(nw);
						}
				}
			}
			this->removeOldPlanes();
		}
	} catch (Ogre::Exception& e) {
		Ogre::LogManager::getSingletonPtr()->logMessage(Ogre::LML_NORMAL,e.getFullDescription());
	}
return true;
}

Plane Stronicowanie::getFirstPlane() {
  std::list<Strona*>::iterator iter = lista.begin();
  return iter.operator *()->getPlane();
}

/*
 * Generates ID for material, Scene and Entiti
 */
Ogre::String Stronicowanie::getNamePref(int x,int y) {
    int locH = getTerrainPtr()->getHeight() / 2;
    int locW = getTerrainPtr()->getWidth() / 2;

    int by=(y*planeElements)+locH;
    int bx=(x*planeElements)+locW;

	ostringstream byStr;
	ostringstream bxStr;

	bxStr << by;
	byStr << bx;

	Ogre::String name = "x:"+bxStr.str()+"y:"+byStr.str();
	return name;
}

int* Stronicowanie::getPlaneXY(int x,int y) {
    int * xx = new int[2];

	xx[0]=round((float)x/planeWeight);
	xx[1]=round((float)y/planeWeight);

    return xx;
}

void  Stronicowanie::drawCircle(long x0, long y0, long r,int height) {

Vector2 pos(x0,y0);
long perc = r;
float pc;
float color=((float)height/150)/2;
for(int y=-r; y<=r; y++)
	for(int x=-r; x<=r; x++)
		if(x*x+y*y <= r*r) {
			Vector2 pos2(x0+x,y0+y);
			Vector2 wyn = pos-pos2;

			pc =1-(wyn.length()/perc);
			if(getSelectedLayer()==0) {
				if(pc<0.10) pc=0;
				getTerrainPtr()->setPixel(x0+x,y0+y,getTerrainPtr()->getPixel(x0+x,y0+y)+height*pc);
			} else {
				ColourValue co = getLayersTexturesPtr()->map.getColourAt(x0+x,y0+y,0);
				if(getSelectedLayer()==1) {
					co.r +=(pc*color);
				}
				if(getSelectedLayer()==2) {
					co.g +=(pc*color);
				}
				if(getSelectedLayer()==3) {
					co.b +=(pc*color);
				}
				if(getSelectedLayer()==0) {
					co.a +=(pc*color);
				}
				getLayersTexturesPtr()->map.setColourAt(co,x0+x,y0+y,0);
			}
		}
}


void Stronicowanie::setAmbient(Ogre::ColourValue am) {
	this->ambient = am;
}
Ogre::ColourValue Stronicowanie::getAmbient() {
	return this->ambient;
}

/*
 * Zwraca pixel na mapie - pixel obrazka
 */
Ogre::Vector2 Stronicowanie::getPixeltoVertexCoords(Ogre::Real x, Ogre::Real y) {
	long cx = floor(x/(planeWeight/planeElements));
	long cy = floor(y/(planeWeight/planeElements));

	cx=getTerrainPtr()->getWidth()/2+cx;
	cy=getTerrainPtr()->getHeight()/2+cy;
	return Ogre::Vector2(cx,cy);
}
/*
 *  Create pointer to player ent and player node
 */
void Stronicowanie::creat3dPointer() {
	Ogre::SceneManager*  mSceneMgr = DataConnectSing::getSingleton()->getDataConnector()->mSceneMgr;
	Ogre::Entity* sphare = mSceneMgr->createEntity("sphare", "sphere.mesh");
	sphare->setCastShadows(false);
	sphare->setQueryFlags(Ogre::Flags::ObjectFlags::MOVING_OBJ);
	sphare->setMaterialName("Template/Red50");

	sphareNode = bigPlain->createChildSceneNode("SphereNode",Ogre::Vector3(0,0,0));
	sphareNode->attachObject(sphare);
	sphareNode->scale(15,15,15);
	sphareNode->setPosition(0,0,0);
	}

SceneNode* Stronicowanie::get3dPointer() {
	return sphareNode;
	}

Stronicowanie::~Stronicowanie() {
	}
}
