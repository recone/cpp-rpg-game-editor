/*
 * MouseActions.h
 *
 *  Created on: 5 gru 2015
 *      Author: rafal
 */

#ifndef SRC_TRAY_MOUSEACTIONS_H_
#define SRC_TRAY_MOUSEACTIONS_H_

#include <OISEvents.h>
#include <OISMouse.h>
#include <OgreEntity.h>
#include <OgreRenderWindow.h>
#include <OgreCamera.h>

#include "../Stronicowanie/basics/MyMovableObject.h"
#include "../Connector/DataConnect.h"
#include "../Stronicowanie/basics/MyRaycast.h"

namespace Ogre {

class MouseActions : public DataConnect {
public:
	Ogre::Ray getFromCameraRay(OIS::MouseEvent arg);

	bool getMouseHitPointGround(OIS::MouseEvent arg, Ogre::Vector3 &hp);
	bool getMouseHitPoint(OIS::MouseEvent arg, Ogre::Vector3 &hp);
	bool getMouseHitPoint(OIS::MouseEvent arg);
	bool mouseOverGround(OIS::MouseEvent arg, Ogre::Vector3 &hp);

	MyMovableObject* getObjectHitBox(OIS::MouseEvent arg, Ogre::Vector3 &hp);
	MyMovableObject* getObjectHitObject(OIS::MouseEvent arg, Ogre::Vector3 &hp);

	Ogre::Camera* mCamera;
	Ogre::SceneManager* mSceneMgr;
};

} /* namespace Ogre */

#endif /* SRC_TRAY_MOUSEACTIONS_H_ */
