/*
 * MouseActions.cpp
 *
 *  Created on: 5 gru 2015
 *      Author: rafal
 */

#include "MouseActions.h"

namespace Ogre {

	Ogre::Ray MouseActions::getFromCameraRay(OIS::MouseEvent arg) {
		Ogre::Real screenWidth  = Ogre::Root::getSingleton().getAutoCreatedWindow()->getWidth();
		Ogre::Real screenHeight = Ogre::Root::getSingleton().getAutoCreatedWindow()->getHeight();

		// convert to 0-1 offset
		Ogre::Real offsetX = arg.state.X.abs / screenWidth;
		Ogre::Real offsetY = arg.state.Y.abs / screenHeight;
		return mCamera->getCameraToViewportRay(offsetX, offsetY);
	}
/*
 * Zwraca objekt jesli nie ziemia
 */
MyMovableObject* MouseActions::getObjectHitBox(OIS::MouseEvent arg, Ogre::Vector3 &hp) {
	Ogre::RaySceneQuery* mRayScnQuery = mSceneMgr->createRayQuery(getFromCameraRay(arg));
	//mRayScnQuery->setQueryMask(GROUND_OBJ);
	mRayScnQuery->setSortByDistance(true);
	Ogre::RaySceneQueryResult &result = mRayScnQuery->execute();

		Ogre::RaySceneQueryResult::iterator itr;
		for(itr = result.begin(); itr != result.end(); itr++)  {
			if(itr->movable->getQueryFlags()!=Ogre::Flags::ObjectFlags::GROUND_OBJ) {
				return (MyMovableObject*)itr->movable;
			}
		}
		return NULL;
	}

bool MouseActions::getMouseHitPointGround(OIS::MouseEvent arg, Ogre::Vector3 &hp) {

	Ogre::Ray mouseRay = getFromCameraRay(arg);

	MyRaycast* rc = new MyRaycast(2);
	rc->setRayMask(Ogre::Flags::ObjectFlags::GROUND_OBJ);
	if(rc->raycastFromPoint(mouseRay,hp)) {
		return true;
		}

	return false;
}

/*
 * Jesli trafiles w podloze/plane to zwroci true i Vektor3
 */
bool MouseActions::getMouseHitPoint(OIS::MouseEvent arg, Ogre::Vector3 &hp){
	Ogre::Ray mouseRay = getFromCameraRay(arg);

	MyRaycast* rc = new MyRaycast(2);
	if(rc->raycastFromPoint(mouseRay,hp))
		return true;
	else
		return false;
}

/*
 * Jesli trafiles w podloze/plane to zwroci true i Vektor3
 */
bool MouseActions::getMouseHitPoint(OIS::MouseEvent arg){
	Ogre::Ray mouseRay = getFromCameraRay(arg);

	Vector3 hp;
	MyRaycast* rc = new MyRaycast(2);
	if(rc->raycastFromPoint(mouseRay,hp))
		return true;
	else
		return false;
}

MyMovableObject* MouseActions::getObjectHitObject(OIS::MouseEvent arg, Ogre::Vector3 &hp) {
	Ogre::Ray mouseRay = getFromCameraRay(arg);

	Entity* ent = NULL;
	MyRaycast* rc = new MyRaycast(5);
	if(rc->raycastFromPoint(mouseRay,hp,ent)) {
		if(ent->getQueryFlags() != Ogre::Flags::ObjectFlags::GROUND_OBJ) {
			return (MyMovableObject*)ent;
		}
	}
	throw std::runtime_error("Thers no object to return");
}

bool  MouseActions::mouseOverGround(OIS::MouseEvent arg, Ogre::Vector3 &hp) {
	Ogre::Ray mouseRay = getFromCameraRay(arg);

	Entity* ent = NULL;
	MyRaycast* rc = new MyRaycast(2);

	if(rc->raycastFromPoint(mouseRay,hp,ent)) {
		if(ent->getQueryFlags()!=Ogre::Flags::ObjectFlags::GROUND_OBJ) {

			return false;
		}
	} else {
		return false;
	}
	return true;
}
} /* namespace Ogre */
